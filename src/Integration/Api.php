<?php
namespace Winker\Integration\Util\Integration;

use Winker\Integration\Util\Http\Client;
use Winker\Integration\Util\Integration\Api\Driver;
use Winker\Integration\Util\Integration\Api\Cache;

class Api extends Client {
    /**
     * @var Driver
     */
    private $driver;

    /**
     * @var Cache
     */
    private $cache;

    public function __construct() {
        $this->driver   = new Driver();
        $this->cache    = new Cache();

        $this->setDriver($this->driver);
    }

    public function getDriver() {
        return $this->driver;
    }

    public function getCache() {
        return $this->cache;
    }

    public function addQueue($uri, $uniqueId, array $params = []) {
        if (is_array($uniqueId)) {
            $ids = [];

            foreach ($uniqueId as $model => $id) {
                $ids[] = "{$model}:{$id}";
            }

            $params['unique_id'] = implode('|', $ids);
        } else {
            $params['unique_id'] = $uniqueId;
        }

        return $this->post($uri, $params);
    }

    /**
     * Auth api
     * If authenticated, we make 10 minutes cache
     *
     * @param $token
     * @return array
     */
    public function auth($token) {
        $cacheToken = $this->cache->getItem("api.auth.token#{$token}");

        if ($cacheToken->isHit()) {
            return $cacheToken->get();
        }

        $this->getDriver()->disableAuthToken();
        $userIntegration = $this->post("/v1/auth?__token={$token}");
        $this->getDriver()->enableAuthToken();

        if (!empty($userIntegration['success'])) {
            $cacheToken->set($userIntegration);
            $this->cache->save($cacheToken);
        }

        return $userIntegration;
    }

    public function authFromRequest() {
        return $this->auth(Token::discovery());
    }
}
