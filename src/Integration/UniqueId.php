<?php
namespace Winker\Integration\Util\Integration;

class UniqueId {
    public static function parse($uniqueId) {
        if (empty($uniqueId)) {
            return [];
        }

        $uniqueId   = urldecode($uniqueId);
        $models     = [];

        foreach (explode('|', $uniqueId) as $row) {
            list($model, $id) = explode(':', $row);

            $models[$model] = $id;
        }

        return $models;
    }
}