<?php
namespace Winker\Integration\Util\Integration\Api;

use Winker\Integration\Util\Http\Client;
use Winker\Integration\Util\Integration\Token;

class Driver extends \Winker\Integration\Util\Http\Client\Driver {
    private $enabledAuthToken = true;

    public function getBaseUri() {
        $value = getenv('WINKER_INTEGRATION_API_URL');

        if (!$value) {
            return 'https://integration.winker.com.br/';
        } else {
            return $value;
        }
    }

    public function beforeRequest(Client $client, $method, $uri, &$options) {
        $token = Token::discovery();

        if ($this->enabledAuthToken && !empty($token)) {
            if (empty($options['headers'])) {
                $options['headers'] = [];
            }

            $options['headers']['X-Auth-Token'] = $token;
        }
    }

    public function enableAuthToken() {
        $this->enabledAuthToken = true;
    }

    public function disableAuthToken() {
        $this->enabledAuthToken = false;
    }
}