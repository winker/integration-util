<?php
namespace Winker\Integration\Util\Integration\Api;

use Psr\Cache\CacheItemInterface;
use Symfony\Component\Cache\Adapter\ApcuAdapter;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class Cache {

    private $instance;

    public function getItem($key) {
        return $this->getInstance()->getItem($key);
    }

    public function save(CacheItemInterface $item) {
        return $this->getInstance()->save($item);
    }

    public function clear() {
        return $this->getInstance()->clear();
    }

    private function getInstance() {
        if (empty($this->instance)) {
            $lifeTime   = 10*60;  // 10 minutes
            $namespace  = 'api_cache';

            if (ApcuAdapter::isSupported()) {
                $this->instance = new ApcuAdapter($namespace, $lifeTime);
            } else {
                $this->instance = new FilesystemAdapter($namespace, $lifeTime);
            }
        }

        return $this->instance;
    }
}