<?php

namespace Winker\Integration\Util\Integration;

class Token {
    public static function discovery() {
        $token = isset($_SERVER['WINKER_INTEGRATION_API_TOKEN']) ? $_SERVER['WINKER_INTEGRATION_API_TOKEN'] : null;

        if (!empty($token)) {
            return $token;
        }

        $token = isset($_GET['__token']) ? $_GET['__token'] : null;

        if (!empty($token)) {
            return $token;
        }

        // Header: X-Auth-Token
        $token = isset($_SERVER['HTTP_X_AUTH_TOKEN']) ? $_SERVER['HTTP_X_AUTH_TOKEN'] : null;

        if (!empty($token)) {
            return $token;
        }

        return null;
    }
}