<?php

declare(strict_types=1);

namespace Winker\Integration\Util\Enum;


/**
 * Class DependencyErrorEnum
 *
 * @package App\Services\Mail
 */
class DependencyErrorEnum implements EnumInterface
{
    use EnumTrait;

    /**
     * Erro de dependencia de Portal
     */
    const Portal         = 'dependency error - Portal integration not localized.';

    /**
     * Erro de dependencia de Division
     */
    const Division       = 'dependency error - Division integration not localized.';

    /**
     * Erro de dependencia de Unit
     */
    const Unit           = 'dependency error - Unit integration not localized.';

    /**
     * Erro de dependencia de BankAccount
     */
    const BankAccount    = 'dependency error - BankAccount integration not localized.';

    /**
     * Erro de dependencia de AccountingPlan
     */
    const AccountingPlan = 'dependency error - AccountingPlan integration not localized.';

    /**
     * Erro de dependencia de AccountingEntry
     */
    const AccountingEntry = 'dependency error - AccountingEntry integration not localized.';

}