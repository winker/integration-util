<?php

declare(strict_types=1);

namespace Winker\Integration\Util\Enum;


/**
 * Interface EnumInterface
 *
 * @package App\Services\Mail
 */
interface EnumInterface
{
    /**
     * @param string $string
     *
     * @return string
     */
    public static function getConstByValue(string $string): string;
}