<?php

declare(strict_types=1);

namespace Winker\Integration\Util\Enum;


/**
 * Trait EnumTrait
 *
 * @package App\Services\Mail
 */
trait EnumTrait
{

    /**
     * @param string $string
     *
     * @return string
     */
    public static function getConstByValue(string $string): string
    {
        $reflector = new \ReflectionClass(get_class());

        foreach ($reflector->getConstants() as $label => $value) {
            if (strpos($string, $value) !== false) {
                return $label;
            }
        }


        return "Não localizado.";
    }

}