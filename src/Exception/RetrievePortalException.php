<?php

declare(strict_types=1);

namespace Winker\Integration\Util\Exception;


use Winker\Integration\Util\Enum\DependencyErrorEnum;


/**
 * Class RetrievePortalException
 *
 * @package Winker\Integration\Util\Exception
 */
class RetrievePortalException extends \Exception
{
    public function __construct($message = '')
    {

        if (empty($message)) {
            $message = DependencyErrorEnum::Portal;
        }
        parent::__construct($message);

    }
}