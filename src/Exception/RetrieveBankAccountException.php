<?php

declare(strict_types=1);

namespace Winker\Integration\Util\Exception;


use Winker\Integration\Util\Enum\DependencyErrorEnum;


/**
 * Class RetrieveBankAccountException
 *
 * @package Winker\Integration\Util\Exception
 */
class RetrieveBankAccountException extends \Exception
{
    public function __construct($message = '')
    {

        if (empty($message)) {
            $message = DependencyErrorEnum::BankAccount;
        }
        parent::__construct($message);

    }
}