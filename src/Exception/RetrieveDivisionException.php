<?php

declare(strict_types=1);

namespace Winker\Integration\Util\Exception;


use Winker\Integration\Util\Enum\DependencyErrorEnum;


/**
 * Class RetrieveDivisionException
 *
 * @package Winker\Integration\Util\Exception
 */
class RetrieveDivisionException extends \Exception
{
    public function __construct($message = '')
    {

        if (empty($message)) {
            $message = DependencyErrorEnum::Division;
        }
        parent::__construct($message);

    }
}