<?php

declare(strict_types=1);

namespace Winker\Integration\Util\Exception;


/**
 * Class ValidateDependencyException
 *
 * @package Winker\Integration\Util\Exception
 */
class ValidateDependencyException extends \Exception
{

}