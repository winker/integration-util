<?php

declare(strict_types=1);

namespace Winker\Integration\Util\Exception;


use Winker\Integration\Util\Enum\DependencyErrorEnum;


/**
 * Class RetrieveUnitException
 *
 * @package Winker\Integration\Util\Exception
 */
class RetrieveAccountingEntryException extends \Exception
{
    public function __construct($message = '')
    {

        if (empty($message)) {
            $message = DependencyErrorEnum::AccountingEntry;
        }
        parent::__construct($message);

    }
}