<?php
namespace Winker\Integration\Util\Http\Exception;

class RequestException extends \Exception {
    public function __construct($errors, $code = null, \Exception $previous = null) {
        parent::__construct(json_encode($errors), $code, $previous);
    }
}