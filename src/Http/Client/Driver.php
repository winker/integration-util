<?php
namespace Winker\Integration\Util\Http\Client;

use GuzzleHttp\Psr7\Response;
use Winker\Integration\Util\Http\Client;

abstract class Driver {

    public $TYPE;

    public function getBaseUri() {

    }

    public function cookiesEnabled() {
        return false;
    }

    public function afterFormatResponse(&$json) {

    }

    public function afterRequest(Response $response, $json) {

    }

    public function beforeRequest(Client $client, $method, $uri, &$options) {

    }

    public function getPageParamName() {
        return 'page';
    }

    public function getMethodDefault() {
        return 'GET';
    }

    public function getWhileNextPageResponseCheck() {
        return function($response) {
            return !empty($response['data']);
        };
    }

    public function getType() {
        return is_null($this->TYPE) ? "HASH" : $this->TYPE;
    }

    public function setType($type) {
        $this->TYPE = $type;
    }
}