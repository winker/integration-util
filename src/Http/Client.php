<?php
namespace Winker\Integration\Util\Http;
use Meng\AsyncSoap\Guzzle\Factory;
use Winker\Integration\Util\Http\Client\Driver;
use Winker\Integration\Util\Http\Client\WsseAuthHeader;
use Winker\Integration\Util\Http\Exception\RequestException;

/**
 * Faz requisições para uma API
 *
 * @package Winker\Integration\Util\Client
 */
class Client {
    /**
     * @var Driver
     */
    private $driver;
    private $timeout = 300;
    private $client;
    private $cookies;
    private $handler;
    private $wsdl;
    private $mockResponsesCollection = [];


    public function post($uri, $params = []) {
        return $this->request('POST', $uri, $params);
    }

    private function parseNullValue($params = []) {
        if (empty($params) || !is_array($params)) {
            return $params;
        }

        foreach ($params as $key => $param) {
            if (is_array($param)) {
                $params[$key] = $this->parseNullValue($param);
                continue;
            }

            if (is_null($param)) {
                $params[$key] = "";
            }
        }

//        $params = $this->utf8ize($params);

        return $params;
    }

    public function utf8ize($d) {
        if (is_array($d)) {
            foreach ($d as $k => $v) {
                $d[$k] = $this->utf8ize($v);
            }
        } else if (is_string ($d)) {
            return utf8_encode($d);
        }
        return $d;
    }

    public function get($uri) {
        return $this->request('GET', $uri, []);
    }

    public function put($uri) {
        return $this->request('PUT', $uri, []);
    }

    public function delete($uri) {
        return $this->request('DELETE', $uri, []);
    }

    public function head($uri) {
        return $this->request('HEAD', $uri, []);
    }

    public function patch($uri) {
        return $this->request('PATCH', $uri, []);
    }

    public function request($method, $uri, $params = []) {
        $options = [];
        $apiType = '';

        if (!empty($params)) {

            if ($this->driver) {
                $apiType = $this->driver->getType();
            }

            $optParam = ($apiType === 'REST') ? 'json' : 'form_params';

            if ($method === 'POST' && $optParam === 'form_params') {
                $params = $this->parseNullValue($params);
            }

            $options[$optParam] = $params;
        }

        if ($this->driver && $this->driver->cookiesEnabled()) {
            if (empty($this->cookies)) {
                $this->cookies = new \GuzzleHttp\Cookie\CookieJar;
            }

            $options['cookies'] = $this->cookies;
        }

        if ($this->driver) {
            $this->driver->beforeRequest($this, $method, $uri, $options);
        }

        try {
            if (is_array($options) && empty(json_encode($options))) {
                $options = $this->utf8ize($options);
            }

            $response    = $this->client()->request($method, "{$this->getBaseUri()}{$uri}", $options);
        } catch (\Exception $e) {
            throw new RequestException([$e->getMessage()], $e->getCode(), $e);
        }

        $json = $this->prepareResponse($response);

        if ($this->driver) {
            $this->driver->afterRequest($response, $json);
        }

        return $json;
    }

    private function prepareResponse($response) {
        $json = $this->formatResponse($response);

        return $json;
    }

    private function formatResponse($response) {
        $json = null;

        if ($this->driver && $this->driver->getType() === 'RAW') {
            $body   = (string) $response->getBody();
            $json   = $body;
        } else if ($response instanceof \GuzzleHttp\Psr7\Response) {
            $body   = (string) $response->getBody();
            $json   = json_decode($body, true);
        } else if ($response instanceof \stdClass) {
            $json = json_decode(json_encode($response), true);
        } else if (is_array($response)) {
            $json = $response;
        } else {
            $json = $response;
        }


        if ($this->driver) {
            $this->driver->afterFormatResponse($json);
        }

        return $json;
    }

    public function setDriver(Driver $driver) {
        $this->driver = $driver;
    }

    public function getDriver() {
        return $this->driver;
    }

    public function getBaseUri() {
        $baseUri = null;

        if (!empty($_GET['enviroment'])) {
            return "http://homologacao-winker.elasticbeanstalk.com/api/";
        }

        if ($this->driver) {
            $baseUri = $this->driver->getBaseUri();

            if (substr($baseUri, -1) !== '/') {
                $baseUri .= '/';
            }
        }
        
        return $baseUri;
    }

    private function client() {
        if (!empty($this->client)) {
            return $this->client;
        }

        $client = new \GuzzleHttp\Client([
            // issue: https://github.com/guzzle/guzzle/issues/1550
            //'base_uri'  => $this->getBaseUri(),
            'timeout'   => $this->timeout,
            'handler'   => $this->handler
        ]);

        if (!empty($this->wsdl)) {
            $factory        = new Factory();
            $this->client   = $factory->create($client, $this->wsdl['url']);
        } else {
            $this->client = $client;
        }

        return $this->client;
    }

    public function whileNextPage($uri, $do) {
        $indexPage      = 1;
        $check          = true;
        $responseCheck  = $this->getWhileNextPageResponseCheck();
        $returns        = [];

        while ($check) {
            $uri        = UrlHelper::build($uri, [$this->getPageParamName() => $indexPage]);
            $response   = $this->request($this->getMethodDefault(), $uri);
            $check      = $responseCheck($response);

            if ($check) {
                $returns[] = $do($response);

                $indexPage++;
            }
        }

        return $returns;
    }

    public function getPageParamName() {
        if ($this->driver) {
            return $this->driver->getPageParamName();
        }
    }

    public function getMethodDefault() {
        if ($this->driver) {
            return $this->driver->getMethodDefault();
        }
    }

    public function getWhileNextPageResponseCheck() {
        if ($this->driver) {
            return $this->driver->getWhileNextPageResponseCheck();
        }
    }

    public function mockResponse($response) {
        $this->mockResponses([$response]);
    }

    public function mockJsonResponse($json) {
        if (is_string($json)) {
            $this->mockResponses([json_decode($json, true)]);
        } else {
            $this->mockResponses([$json]);
        }
    }

    public function mockJsonResponses(array $jsons) {

        $responses = [];

        foreach ($jsons as $json) {
            if (is_string($json)) {
                $responses[] = json_decode($json, true);
            } else {
                $responses[] = $json;
            }
        }

        $this->mockResponses($responses);
    }

    public function mockResponses(array $responses) {
        $handlersResponses = [];

        $this->mockResponsesCollection = array_merge($responses);

        foreach ($responses as $response) {
            if (is_array($response)) {
                $handlersResponses[] = new \GuzzleHttp\Psr7\Response(200, [], json_encode($response));
            } else if (is_object($response)) {
                $handlersResponses[] = $response;
            } else if (is_string($response)) {
                $handlersResponses[] = new \GuzzleHttp\Psr7\Response(200, [], json_encode($response));
            }
        }

        $mock = new \GuzzleHttp\Handler\MockHandler($handlersResponses);

        $this->handler = \GuzzleHttp\HandlerStack::create($mock);
    }

    public function getWsdl() {
        return $this->wsdl;
    }

    public function setWsdl($url, $username = null, $password = null) {
        $wsse = null;

        if (!empty($username) && !empty($password)) {
            $wsse = new WsseAuthHeader($username, $password);
        }

        $this->wsdl = [
            'url'   => $url,
            'wsse'  => $wsse,
        ];
    }

    /**
     * @todo Driver
     */
    public function call($name, array $arguments, array $options = null, $inputHeaders = [], array &$outputHeaders = null) {

//        if ($this->driver) {
//            $this->driver->beforeRequest($this, $method, $uri, $options);
//        }

        $response = null;

        if (empty($this->mockResponsesCollection)) {

            if (!empty($this->wsdl['wsse'])) {
                $inputHeaders[] = $this->wsdl['wsse'];
            }

            $response = $this->client()->call($name, $arguments, $options, $inputHeaders, $outputHeaders);
        } else {
            $response = array_shift($this->mockResponsesCollection);
        }

        $json = $this->prepareResponse($response);

//        if ($this->driver) {
//            $this->driver->afterRequest($response, $json);
//        }

        return $json;
    }
}
