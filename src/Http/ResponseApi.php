<?php

namespace Winker\Integration\Util\Http;


use Winker\Integration\Util\Http\Exception\RequestException;

abstract class ResponseApi {

    private static $debug = false;

    public static function success($data = null) {

        return [
            'success' => true,
            'data' => $data
        ];
    }

    public static function error($errors = null) {

        $trace = null;

        if (!empty($errors)) {

            $errors = self::parseError($errors);
        }

        if (!self::$debug && !empty($result['trace'])) {
            unset($result['trace']);
        }

        $response = array_merge( ['success' => false], (array) $errors );

        return $response;
    }

    private static function parseError($errors) {

        if (is_object($errors)) {

            if ($errors instanceof RequestException) {
                return [
                    'trace'  => $errors->getTrace(),
                    'errors' => json_decode($errors->getMessage())
                ];
            }

            if ($errors instanceof \Exception) {

                return [
                    'trace'  => $errors->getTrace(),
                    'errors' => [
                        $errors->getMessage()
                    ]
                ];
            }

            return [
                'errors' => ['Inválid Error']
            ];
        }

        if (is_string($errors)) {
            return [
                'trace'  => null,
                'errors' => [
                    $errors
                ]
            ];
        }

        if (is_array($errors)) {
            return [
                'trace'  => isset($errors["trace"]) ? $errors["trace"] : null,
                'errors' => isset($errors["trace"]) ? $errors["message"] : $errors,
            ];
        }
    }

    public static function fromMultiplesResponses($responses) {

        $hasSuccess = false;
        $data       = [];
        $errors     = [];

        foreach ($responses as $response) {
            if (!empty($response['success'])) {
                $hasSuccess = true;
            }

            if (!empty($response['data'])) {
                $data = array_merge($data, $response['data']);
            }

            if (!empty($response['errors'])) {
                $errors = array_merge($errors, $response['errors']);
            }
        }

        $return = [
            'success' => $hasSuccess,
        ];

        if (!empty($data)) {
            $return['data']   = $data;
        }

        if (!empty($errors)) {
            $return['errors'] = $errors;
        }

        return $return;
    }

    public static function enableDebug() {

        self::$debug = true;
    }

    public static function disableDebug() {

        self::$debug = false;
    }
}