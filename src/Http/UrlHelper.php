<?php
namespace Winker\Integration\Util\Http;

class UrlHelper {
    public static function build($url, $params) {
        $parsed = parse_url($url);

        if (empty($parsed['query'])) {
            $parsed['query'] = [];
        }

        $paramsUrl = self::parse_query($parsed['query']);

        $parsed['query'] = self::unparse_query(array_merge($paramsUrl, $params));

        return self::unparse_url($parsed);
    }

    public static function parse_query($query) {
        $paramsUrl = [];

        if (empty($query)) {
            return $paramsUrl;
        }

        foreach (explode('&', $query) as $queryParam) {
            list($key, $value) = explode('=', $queryParam);

            $paramsUrl[$key] = $value;
        }

        return $paramsUrl;
    }

    public static function unparse_query($parsed_query) {
        if (empty($parsed_query)) {
            return '';
        }

        $params = [];

        foreach ($parsed_query as $key => $value) {
            $params[] = "{$key}={$value}";
        }

        return implode('&', $params);
    }

    public static function unparse_url($parsed_url) {
        $scheme   = !empty($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
        $host     = !empty($parsed_url['host']) ? $parsed_url['host'] : '';
        $port     = !empty($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
        $user     = !empty($parsed_url['user']) ? $parsed_url['user'] : '';
        $pass     = !empty($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
        $pass     = ($user || $pass) ? "$pass@" : '';
        $path     = !empty($parsed_url['path']) ? $parsed_url['path'] : '';
        $query    = !empty($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
        $fragment = !empty($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
        return "$scheme$user$pass$host$port$path$query$fragment";
    }
}