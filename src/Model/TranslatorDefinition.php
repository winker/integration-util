<?php
namespace Winker\Integration\Util\Model;

use Winker\Integration\Util\Http\Client;
use Winker\Integration\Util\Integration\Api;
use Winker\Integration\Util\Model\Translation;

abstract class TranslatorDefinition {
    abstract public function winkerModelTranslation();
    abstract public function winkerFieldsTranslation();

    abstract public function vendorModelTranslation();
    abstract public function vendorFieldsTranslation();

    public function toWinkerList(array $data) {
        return Translation::translateList($data, $this->winkerFieldsTranslation(), $this->winkerModelTranslation());
    }

    public function toWinker(array $data) {
        return Translation::translate($data, $this->winkerFieldsTranslation(), $this->winkerModelTranslation());
    }

    public function toVendorList(array $data) {
        return Translation::translateList($data, $this->vendorFieldsTranslation(), $this->vendorModelTranslation());
    }

    public function toVendor(array $data) {
        return Translation::translate($data, $this->vendorFieldsTranslation(), $this->vendorModelTranslation());
    }

    public function extractUniqueId($data) {
        $model = $this->toWinker($data);

        if (!empty($model['unique_id'])) {
            return $model['unique_id'];
        } else {
            return null;
        }
    }
}