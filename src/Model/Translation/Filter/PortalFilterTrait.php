<?php

declare(strict_types=1);

namespace Winker\Integration\Util\Model\Translation\Filter;


use Winker\Integration\Util\Exception\RetrievePortalException;

/**
 * Trait PortalFilterTrait
 *
 * @package Winker\Integration\Util\Model\Translation\Filter
 */
trait PortalFilterTrait
{

    use FilterStringTrait;

    /**
     * @param string $filterString
     *
     * @return string
     * @throws RetrievePortalException
     */
    public function filterExternalIdPortalGetId(string $filterString): string
    {

        $filteredString = $this->filterExternalId($filterString);

        $this->setFilterString($filteredString);

        if (preg_match("/(.*)(Portal:([\w\s+\-\/])+)/", $filteredString)) {
            return $this->filterExternalIdPortalSuperLogicaId();
        }

        if (preg_match("/(.*)(Condominio#([\w\s+\-\/])+)/", $filterString)) {
            return $this->filterExternalIdPortalId();
        }

        return $this->filterExternalIdPortalFusionId();

    }

    /**
     * @param string $filterString
     *
     * @return string
     * @throws RetrievePortalException
     */
    public function filterExternalId(string $filterString): string
    {

        $this->setFilterString($filterString);

        if (preg_match("/(.*)(Portal:([\w\s+\-\/])+)/", $filterString)) {
            return $this->filterExternalIdPortalSuperLogica();
        }

        if (preg_match("/(.*)(Condominio#([\w\s+\-\/])+)/", $filterString)) {
            return $this->filterExternalIdPortal();
        }

        return $this->filterExternalIdPortalFusion();

    }

    /**
     * @return string
     * @throws RetrievePortalException
     */
    private function filterExternalIdPortal(): string
    {

        $match = null;
        preg_match(
            "/(.*)(Condominio#([\w\s+\-\/])+)/", $this->getFilterString(),
            $match
        );

        if (empty($match)) {
            throw new RetrievePortalException('External id inválido');
        }

        return $match[0];
    }

    /**
     * @return string
     * @throws RetrievePortalException
     */
    private function filterExternalIdPortalId(): string
    {

        $match = preg_replace(
            "/(.*)(Condominio#)/", '', $this->getFilterString()
        );

        if (empty($match)) {
            throw new RetrievePortalException('External id inválido');
        }

        return $match;
    }

    /**
     * @return string
     * @throws RetrievePortalException
     */
    private function filterExternalIdPortalSuperLogica(): string
    {

        $match = null;
        preg_match(
            "/(.*)(Portal:([\w\s+\-\/])+)/", $this->getFilterString(), $match
        );

        if (empty($match)) {
            throw new RetrievePortalException('External id inválido');
        }

        return $match[0];
    }

    /**
     * @return string
     * @throws RetrievePortalException
     */
    private function filterExternalIdPortalSuperLogicaId(): string
    {

        $match = preg_replace(
            "/(.*)(Portal:)/", '', $this->getFilterString()
        );

        if (empty($match)) {
            throw new RetrievePortalException('External id inválido');
        }

        return $match;
    }

    /**
     * @return string
     * @throws RetrievePortalException
     */
    private function filterExternalIdPortalFusion(): string
    {

        $match = null;
        preg_match(
            "/[^\.]+/", $this->getFilterString(),
            $match
        );

        if (empty($match)) {
            throw new RetrievePortalException('External id inválido');
        }

        return $match[0];
    }

    /**
     * @return string
     * @throws RetrievePortalException
     */
    private function filterExternalIdPortalFusionId(): string
    {

        $match = null;
        preg_match(
            "/[^\.]+/", $this->getFilterString(),
            $match
        );

        if (empty($match)) {
            throw new RetrievePortalException('External id inválido');
        }

        $match = explode('|',$match[0]);
        return $match[1];
    }


}
