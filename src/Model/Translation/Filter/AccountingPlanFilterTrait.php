<?php

declare(strict_types=1);

namespace Winker\Integration\Util\Model\Translation\Filter;


use Winker\Integration\Util\Exception\RetrieveAccountingPlanException;


/**
 * Trait AccountingPlanFilterTrait
 *
 * @package Winker\Integration\Util\Model\Translation\Filter
 */
trait AccountingPlanFilterTrait
{

    use FilterStringTrait;

    /**
     * @param string $filterString
     *
     * @return string
     * @throws RetrieveAccountingPlanException
     */
    public function filterExternalId(string $filterString): string
    {

        $this->setFilterString($filterString);

        if (preg_match("/(.*)(\|AccountingPlan:([\w\s+\-\/])+)/", $filterString)) {
            return $this->filterExternalIdAccountingPlanSuperLogica();
        }

        return $this->filterExternalIdAccountingPlan();

    }

    /**
     * @return string
     * @throws RetrieveAccountingPlanException
     */
    private function filterExternalIdAccountingPlan(): string
    {

        $match = null;
        preg_match(
            "/(.*)(PlanoContaContabilidade#([\w\s+\-\/])+)/", $this->getFilterString(), $match
        );

        if (empty($match)) {
            throw new RetrieveAccountingPlanException('External id inválido');
        }

        return $match[0];
    }

    /**
     * @return string
     * @throws RetrieveAccountingPlanException
     */
    private function filterExternalIdAccountingPlanSuperLogica(): string
    {

        $match = null;
        preg_match(
            "/(.*)(\|AccountingPlan:([\w\s+\-\/])+)/", $this->getFilterString(), $match
        );

        if (empty($match)) {
            throw new RetrieveAccountingPlanException('External id inválido');
        }

        return $match[0];
    }

}
