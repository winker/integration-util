<?php

declare(strict_types=1);

namespace Winker\Integration\Util\Model\Translation\Filter;


use Winker\Integration\Util\Exception\RetrieveUnitException;


/**
 * Trait UnitFilterTrait
 *
 * @package Winker\Integration\Util\Model\Translation\Filter
 */
trait UnitFilterTrait
{

    use FilterStringTrait;

    /**
     * @param string $filterString
     *
     * @return string
     * @throws RetrieveUnitException
     */
    public function filterExternalId(string $filterString): string
    {

        $this->setFilterString($filterString);

        if (preg_match("/(.*)(\|Unit:([\w\s+\-\/])+)/", $filterString)) {
            return $this->filterExternalIdUnitSuperLogica();
        }

        return $this->filterExternalIdUnit();

    }

    /**
     * @return string
     * @throws RetrieveUnitException
     */
    private function filterExternalIdUnit(): string
    {

        $match = null;
        preg_match(
            "/(.*)(Unidade#([\w\s+\-\/])+)/", $this->getFilterString(), $match
        );

        if (empty($match)) {
            throw new RetrieveUnitException('External id inválido');
        }

        return $match[0];
    }

    /**
     * @return string
     * @throws RetrieveUnitException
     */
    private function filterExternalIdUnitSuperLogica(): string
    {

        $match = null;
        preg_match(
            "/(.*)(\|Unit:([\w\s+\-\/])+)/", $this->getFilterString(), $match
        );

        if (empty($match)) {
            throw new RetrieveUnitException('External id inválido');
        }

        return $match[0];
    }

}
