<?php

declare(strict_types=1);

namespace Winker\Integration\Util\Model\Translation\Filter;


/**
 * Interface FilterInterface
 *
 * @package Winker\Integration\Util\Model\Translation\Filter
 */
interface FilterInterface
{

    /**
     * @param string $filterString
     *
     * @return mixed
     */
    public function filterExternalId(string $filterString);

    /**
     * @return string
     */
    public function getFilterString(): string;

    /**
     * @param string $filterString
     *
     * @return mixed
     */
    public function setFilterString(string $filterString);
}