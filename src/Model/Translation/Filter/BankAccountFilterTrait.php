<?php

declare(strict_types=1);

namespace Winker\Integration\Util\Model\Translation\Filter;


use Winker\Integration\Util\Exception\RetrieveBankAccountException;


/**
 * Trait BankAccountFilterTrait
 *
 * @package Winker\Integration\Util\Model\Translation\Filter
 */
trait BankAccountFilterTrait
{

    use FilterStringTrait;

    /**
     * @param string $filterString
     *
     * @return string
     * @throws RetrieveBankAccountException
     */
    public function filterExternalId(string $filterString): string
    {

        $this->setFilterString($filterString);

        if (preg_match("/(.*)(BankAccount:([\w\s+\-\/])+)/", $filterString)) {
            return $this->filterExternalIdBankAccountSuperLogica();
        }

        return $this->filterExternalIdBankAccount();

    }

    /**
     * @return string
     * @throws RetrieveBankAccountException
     */
    private function filterExternalIdBankAccount(): string
    {

        $match = null;
        preg_match(
            "/(.*)(Banco#([\w\s+\-\/])+)/", $this->getFilterString(), $match
        );

        if (empty($match)) {
            throw new RetrieveBankAccountException('External id inválido');
        }

        return $match[0];
    }

    /**
     * @return string
     * @throws RetrieveBankAccountException
     */
    private function filterExternalIdBankAccountSuperLogica(): string
    {

        $match = null;
        preg_match(
            "/(.*)(BankAccount:([\w\s+\-\/])+)/", $this->getFilterString(),
            $match
        );

        if (empty($match)) {
            throw new RetrieveBankAccountException('External id inválido');
        }

        return $match[0];
    }
}
