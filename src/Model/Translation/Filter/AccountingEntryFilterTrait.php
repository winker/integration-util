<?php

declare(strict_types=1);

namespace Winker\Integration\Util\Model\Translation\Filter;


use Winker\Integration\Util\Exception\RetrieveAccountingEntryException;


/**
 * Trait AccountingEntryFilterTrait
 *
 * @package Winker\Integration\Util\Model\Translation\Filter
 */
trait AccountingEntryFilterTrait
{

    use FilterStringTrait;

    /**
     * @param string $filterString
     *
     * @return string
     * @throws RetrieveAccountingEntryException
     */
    public function filterExternalId(string $filterString): string
    {

        $this->setFilterString($filterString);

        if (preg_match("/(.*)(\|AccountingEntry:([\w\s+\-\/])+)/", $filterString)) {
            return $this->filterExternalIdAccountingEntrySuperLogica();
        }

        return $this->filterExternalIdAccountingEntry();

    }

    /**
     * @return string
     * @throws RetrieveAccountingEntryException
     */
    private function filterExternalIdAccountingEntry(): string
    {

        $match = null;
        preg_match(
            "/(.*)(LancamentoContabil#([\w\s+\-\/])+)/", $this->getFilterString(), $match
        );

        if (empty($match)) {
            throw new RetrieveAccountingEntryException('External id inválido');
        }

        return $match[0];
    }

    /**
     * @return string
     * @throws RetrieveAccountingEntryException
     */
    private function filterExternalIdAccountingEntrySuperLogica(): string
    {

        $match = null;
        preg_match(
            "/(.*)(\|AccountingEntry:([\w\s+\-\/])+)/", $this->getFilterString(), $match
        );

        if (empty($match)) {
            throw new RetrieveAccountingEntryException('External id inválido');
        }

        return $match[0];
    }

}
