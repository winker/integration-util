<?php

declare(strict_types=1);

namespace Winker\Integration\Util\Model\Translation\Filter;

/**
 * Trait FilterStringTrait
 *
 * @package Winker\Integration\Util\Model\Translation\Filter
 */
trait FilterStringTrait
{

    /**
     * @var string
     */
    private $filterString;

    /**
     * @return string
     */
    public function getFilterString(): string
    {
        return $this->filterString;
    }

    /**
     * @param string $filterString
     */
    public function setFilterString(string $filterString)
    {
        $this->filterString = $filterString;
    }

}