<?php

declare(strict_types=1);

namespace Winker\Integration\Util\Model\Translation\Filter;


use Winker\Integration\Util\Exception\RetrieveDivisionException;

/**
 * Trait DivisionFilterTrait
 *
 * @package Winker\Integration\Util\Model\Translation\Filter
 */
trait DivisionFilterTrait
{

    use FilterStringTrait;

    /**
     * @param string $filterString
     *
     * @return string
     * @throws RetrieveDivisionException
     */
    public function filterExternalId(string $filterString): string
    {

        $this->setFilterString($filterString);

        return $this->filterExternalIdDivision();
    }

    /**
     * @return string
     * @throws RetrieveDivisionException
     */
    private function filterExternalIdDivision(): string
    {

        $match = null;
        preg_match(
            "/(.*)(Divisao#([\w\s+\-\/])+)/", $this->getFilterString(), $match
        );

        if (empty($match)) {
            throw new RetrieveDivisionException('External id inválido');
        }

        return $match[0];
    }

}
