<?php
namespace Winker\Integration\Util\Model\Translation;

class ExceptionTranslation {
    private $exceptionDictionary;

    public function setExceptionDictionary($exceptionDictionary){
        $this->exceptionDictionary = $exceptionDictionary;
    }

    public function translateErrors($error){
        foreach($this->exceptionDictionary as $exception => $errorMessage) {
            if(stripos($error, $exception) !== FALSE) {
                return $errorMessage;
            }
        }
    }
}