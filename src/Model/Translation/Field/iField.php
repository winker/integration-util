<?php
namespace Winker\Integration\Util\Model\Translation\Field;

interface iField {
    public static function translate($value);
}