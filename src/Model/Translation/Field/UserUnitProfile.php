<?php
namespace Winker\Integration\Util\Model\Translation\Field;

class UserUnitProfile implements iField {
    public static function translate($value) {
        $value = self::normalize($value);

        $options = [
            'proprietario'  => 'Owner',
            'dono'          => 'Owner',
            'inquilino'     => 'Tenant',
            'temporario'    => 'Tenant',
        ];

        foreach ($options as $option => $profile) {
            if ($value === $option) {
                return $profile;
            }
        }

        return null;
    }

    private static function normalize($value) {
        $value = preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"), $value);

        return strtolower(preg_replace('/\W/', '', $value));
    }
}

