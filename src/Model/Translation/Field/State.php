<?php
namespace Winker\Integration\Util\Model\Translation\Field;

class State implements iField {
    public static function translate($value) {
        $position = null;

        if (is_numeric($value)) {
            $position = 0;
        } else {
            $position = 2;
        }

        $states = [
            [12,'AC','Acre'],
            [27,'AL','Alagoas'],
            [13,'AM','Amazonas'],
            [16,'AP','Amapá'],
            [29,'BA','Bahia'],
            [23,'CE','Ceará'],
            [53,'DF','Distrito Federal'],
            [32,'ES','Espírito Santo'],
            [52,'GO','Goiás'],
            [21,'MA','Maranhão'],
            [31,'MG','Minas Gerais'],
            [50,'MS','Mato Grosso do Sul'],
            [51,'MT','Mato Grosso'],
            [15,'PA','Pará'],
            [25,'PB','Paraíba'],
            [26,'PE','Pernambuco'],
            [22,'PI','Piauí'],
            [41,'PR','Paraná'],
            [33,'RJ','Rio de Janeiro'],
            [24,'RN','Rio Grande do Norte'],
            [11,'RO','Rondônia'],
            [14,'RR','Roraima'],
            [43,'RS','Rio Grande do Sul'],
            [42,'SC','Santa Catarina'],
            [28,'SE','Sergipe'],
            [35,'SP','São Paulo'],
            [17,'TO','Tocantis'],
        ];

        if ($position === 0) {
            foreach ($states as $stateOpts) {
                if ($stateOpts[$position] == $value) {
                    return $stateOpts[1];
                }
            }
        } else if ($position === 2) {
            $valueNormalized = self::normalize($value);

            foreach ($states as $stateOpts) {
                if (self::normalize($stateOpts[$position]) == $valueNormalized) {
                    return $stateOpts[1];
                }
            }
        }

        return $value;
    }

    private static function normalize($value) {
        $value = preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"), $value);

        return strtolower(preg_replace('/\W/', '', $value));
    }
}

