<?php
namespace Winker\Integration\Util\Model\Translation\Field;

class NumberWithoutDigit implements iField {
    public static function translate($value) {
        if (empty($value)) {
            return;
        }

        $array = preg_split(NumberDigit::$SPLIT_PATTERN, $value);

        return current($array);
    }
}