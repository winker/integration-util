<?php
namespace Winker\Integration\Util\Model\Translation\Field;

class NumberDigit implements iField {
    public static $SPLIT_PATTERN = '@[-_ /:\@#]@';

    public static function translate($value) {
        if (empty($value)) {
            return;
        }

        $array = preg_split(self::$SPLIT_PATTERN, $value);

        if (count($array) < 2) {
            return;
        }

        return end($array);
    }
}