<?php
namespace Winker\Integration\Util\Model\Translation\Field;

class Zipcode implements iField {
    public static function translate($value) {
        return preg_replace('/[^0-9]/', '', $value);
    }
}