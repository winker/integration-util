<?php
namespace Winker\Integration\Util\Model\Translation\Model;

use Winker\Integration\Util\Model\Translation\Field\CNPJ;
use Winker\Integration\Util\Model\Translation\Field\State;
use Winker\Integration\Util\Model\Translation\Field\Zipcode;

class Portal extends Model {
    public static function unique_id() {
        return ['Portal' => 'id_portal'];
    }

    public static function fields() {
        return [
            'id_portal'             => null,
            'name'                  => null,
            'doc_cnpj'              => CNPJ::class,
            'phone'                 => null,
            'address'                => null,
            'address_complement'     => null,
            'address_neighborhood'   => null,
            'address_city'           => null,
            'address_state'          => State::class,
            'address_zipcode'        => Zipcode::class,
            'email_portal'          => null,
            'billing_due_date'      => null,
        ];
    }

    public static function relations() {
        return [];
    }
}