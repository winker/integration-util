<?php
namespace Winker\Integration\Util\Model\Translation\Model;

class BillingUnit extends Model {
    public static function unique_id() {
        return ['Portal' => 'id_portal', 'Unit' => 'id_unit', 'BillingUnit' => 'id_billing_unit'];
    }

    public static function fields() {
        return [
            'id_portal'             => null,
            'id_unit'               => null,
            'id_bank_account'       => null,
            'id_billing_unit'       => null,

            'amount'            => null,
            'due_date'          => null,
            'created_date'      => null,
            'reference_date'    => null,
            'cancel_date'       => null,
            'document_number'   => null,

            'url_billing_file'  => null,

            'our_number'            => null,
            'our_number_formated'   => null,

            'payment_date'          => null,
            'amount_paid'           => null,
            'amount_fine_paid'      => null,
            'amount_interest_paid'  => null,
            'amount_discount_paid'  => null,
            'amount_restatement'    => null,
            'sequence_number'       => null
        ];
    }

    public static function relations() {
        return ['Portal' => Portal::unique_id(), 'Unit' => Unit::unique_id(), 'BankAccount' => BankAccount::unique_id()];
    }
}