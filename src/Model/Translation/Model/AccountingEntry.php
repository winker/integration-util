<?php
namespace Winker\Integration\Util\Model\Translation\Model;

class AccountingEntry extends Model {
    public static $SUPERLOGICA_TIPO_RECEITA = 1;
    public static $SUPERLOGICA_TIPO_DESPESA = 2;
    public static $SUPERLOGICA_TIPO_RENDIMENTO = 3;

    public static function unique_id() {
        return ['Portal' => 'id_portal', 'AccountingPlan' => 'id_accounting_plan', 'AccountingEntry' => 'id_accounting_entry'];
    }

    public static function fields() {
        return [
            'id_accounting_entry'   => null,
            'id_accounting_plan'    => null,
            'id_portal'             => null,
            'classification'        => null,
            'description'           => null,
            'entry_date'            => null,
            'files'                 => null,
            'value'                 => null,
            'enabled'               => null,
        ];
    }

    public static function relations() {
        return ['Portal' => Portal::unique_id(), 'AccountingPlan' => AccountingPlan::unique_id()];
    }
}
