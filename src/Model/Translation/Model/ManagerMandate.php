<?php
namespace Winker\Integration\Util\Model\Translation\Model;

class ManagerMandate extends Model {
    public static function unique_id() {
        return ['Portal' => 'id_portal', 'ManagerMandate' => 'id_manager_mandate'];
    }

    public static function fields() {
        return [
            'id_manager_mandate'    => null,
            'id_portal'             => null,
            'start'                 => null,
            'end'                   => null,
            'email'                 => null,
            'responsability'        => null,
            'first_name'            => null,
            'last_name'             => null,
            'cellphone'             => null,
            'phone'                 => null,
            'status'                => null
        ];
    }

    public static function relations() {
        return ['Portal' => Portal::unique_id()];
    }
}