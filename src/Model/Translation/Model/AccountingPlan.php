<?php
namespace Winker\Integration\Util\Model\Translation\Model;

class AccountingPlan extends Model {
    public static function unique_id() {
        return ['Portal' => 'id_portal', 'AccountingPlan' => 'id_accounting_plan'];
    }

    public static function fields() {
        return [
            'id_accounting_plan'            => null,
            'id_portal'                     => null,
            'name'                          => null,
            'classification'                => null,
            'enabled'                       => null,
        ];
    }

    public static function relations() {
        return ['Portal' => Portal::unique_id()];
    }
}