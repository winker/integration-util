<?php
namespace Winker\Integration\Util\Model\Translation\Model;

use Winker\Integration\Util\Model\Translation\Exception;

abstract class Model {
    abstract public static function unique_id();
    abstract public static function fields();
    abstract public static function relations();

    public static function translateField($field, $value) {
        return self::exec(static::fields(), $field, $value);
    }

    public static function translateFieldCustomModel($field, $value, $customModel) {
        $fields = static::fields();

        foreach ($customModel->fields as $fieldKey => $fieldCustom) {
            $fields[$fieldKey] = $fieldCustom;
        }

        return self::exec($fields, $field, $value);
    }

    private static function exec($fields, $field, $value) {
        if (!array_key_exists($field, $fields)) {
            throw new Exception("Field {$field} not found.");
        }

        $translator = $fields[$field];

        if (empty($translator)) {
            return $value;
        } else {
            if (is_string($translator)) {
                return $translator::translate($value);
            } else {
                return $translator($value);
            }
        }
    }
}