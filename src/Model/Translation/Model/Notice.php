<?php
namespace Winker\Integration\Util\Model\Translation\Model;

class Notice extends Model {
    public static function unique_id() {
        return ['Portal' => 'id_portal', 'Notice' => 'id_notice'];
    }

    public static function fields() {
        return [
            'title'       => null,
            'content'     => null,
            'id_portal'   => null,
        ];
    }

    public static function relations() {
        return ['Portal' => Portal::unique_id()];
    }
}