<?php
namespace Winker\Integration\Util\Model\Translation\Model;

use Winker\Integration\Util\Model\Translation\Field\CNPJ;
use Winker\Integration\Util\Model\Translation\Field\NumberDigit;
use Winker\Integration\Util\Model\Translation\Field\NumberWithoutDigit;
use Winker\Integration\Util\Model\Translation\Field\State;
use Winker\Integration\Util\Model\Translation\Field\Zipcode;

class BankAccount extends Model {
    public static function unique_id() {
        return ['Portal' => 'id_portal', 'BankAccount' => 'id_bank_account'];
    }

    public static function fields() {
        return [
            'agency_number'                 => NumberWithoutDigit::class,
            'agency_number_digit'           => NumberDigit::class,
            'agency_address'                => null,
            'currency'                      => null,
            'name'                          => null,
            'bank_number'                   => null,
            'id_bank_account'               => null,
            'id_portal'                     => null,
            'balance'                       => null,
            'account_number'                => NumberWithoutDigit::class,
            'account_number_digit'          => NumberDigit::class,
            'account_approved'              => null,
            'account_description'           => null,
            'accepted'                      => null,
            'billing_wallet'                => null,
            'billing_document_currency'     => null,
            'billing_payment_place'         => null,
            'billing_assignor_name'         => null,
            'billing_layout_type'           => null,
            'billing_bank_agreement'        => NumberWithoutDigit::class,
            'billing_bank_agreement_digit'  => NumberDigit::class,
            'enabled'                       => null,
        ];
    }

    public static function relations() {
        return ['Portal' => Portal::unique_id()];
    }
}