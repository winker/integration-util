<?php
namespace Winker\Integration\Util\Model\Translation\Model;

class Unit extends Model {
    public static function unique_id() {
        return ['Portal' => 'id_portal', 'Unit' => 'id_unit'];
    }

    public static function fields() {
        return [
            'id_unit'               => null,
            'id_portal'             => null,
            'id_division'           => null,
            'division'              => null,
            'name'                  => null,
            'fractional_ownership'  => null,
        ];
    }

    public static function relations() {
        return ['Portal' => Portal::unique_id()];
    }
}