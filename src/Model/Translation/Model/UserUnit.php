<?php
namespace Winker\Integration\Util\Model\Translation\Model;

use Winker\Integration\Util\Model\Translation\Field\CPF;
use Winker\Integration\Util\Model\Translation\Field\State;
use Winker\Integration\Util\Model\Translation\Field\UserUnitProfile;
use Winker\Integration\Util\Model\Translation\Field\Zipcode;

class UserUnit extends Model {
    public static function unique_id() {
        return ['Portal' => 'id_portal', 'Unit' => 'id_unit', 'UserUnit' => 'id_user_unit'];
    }

    public static function fields() {
        return [
            'id_unit'               => null,
            'unit_name'             => null,
            'division_name'         => null,
            'id_portal'             => null,
            'id_user_unit'          => null,
            'name'                  => null,
            'phone'                 => null,
            'cellphone'             => null,
            'email'                 => null,
            'doc_cpf'               => CPF::class,
            'doc_rg'                => null,
            'profile'               => UserUnitProfile::class,
            'address'                => null,
            'address_neighborhood'   => null,
            'address_zipcode'        => Zipcode::class,
            'address_city'           => null,
            'address_state'          => State::class,
        ];
    }

    public static function relations() {
        return ['Portal' => Portal::unique_id(), 'Unit' => Unit::unique_id()];
    }
}