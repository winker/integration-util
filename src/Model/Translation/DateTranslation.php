<?php
namespace Winker\Integration\Util\Model\Translation;

class DateTranslation {
    private $from_format;
    private $to_format;
    private $field;

    public function __construct($format, $field = null) {
        if (is_array($format)) {
            $this->from_format  = key($format);
            $this->to_format    = current($format);
        } else {
            $this->from_format  = $format;
            $this->to_format    = 'Y-m-d';
        }

        $this->field = $field;
    }

    public function translate($value) {
        if (empty($value)) {
            return;
        }

        $format = $this->from_format;

        // value with date and time
        if (preg_match('@^\d{1,4}[/-]?\d{1,4}[/-]?\d{1,4} \d{2}:\d{2}:\d{2}$@', $value)) {
            // format with only date
            if (preg_match('@\w[/-]?\w[/-]?\w@', $format)) {
                $format = "{$format} H:i:s";
            }
        }

        $dateTime = \DateTime::createFromFormat($format, $value);

        if (!$dateTime) {
            throw new Exception("Invalid date \"{$value}\" with \"{$format}\" format.");
        }

        return $dateTime->format($this->to_format);
    }

    public function translateFromData($data) {
        if (empty($data)) {
            return null;
        }

        if (empty($this->field)) {
            return null;
        }

        if (!array_key_exists($this->field, $data)) {
            return null;
        }

        return $this->translate($data[$this->field]);
    }

    public function getField() {
        return $this->field;
    }
}