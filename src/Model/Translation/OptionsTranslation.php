<?php
namespace Winker\Integration\Util\Model\Translation;

class OptionsTranslation {
    private $options;
    private $field;

    public function __construct($options, $field = null) {
        $this->options  = $options;
        $this->field    = $field;
    }

    public function translate($value) {
        if (empty($this->options)) {
            return null;
        }

        foreach ($this->options as $option => $optionValue) {
            if ($option == $value) {
                return $optionValue;
            }
        }

        return null;
    }

    public function translateFromData($data) {
        if (empty($data)) {
            return null;
        }

        if (empty($this->field)) {
            return null;
        }

        if (!array_key_exists($this->field, $data)) {
            return null;
        }

        return $this->translate($data[$this->field]);
    }

    public function getField() {
        return $this->field;
    }
}