<?php
namespace Winker\Integration\Util\Model;
use Winker\Integration\Util\Integration\UniqueId;
use Winker\Integration\Util\Model\Translation\DateTranslation;
use Winker\Integration\Util\Model\Translation\Exception;
use Winker\Integration\Util\Model\Translation\Model\CustomModel;
use Winker\Integration\Util\Model\Translation\Model\Model;
use Winker\Integration\Util\Model\Translation\OptionsTranslation;

/**
 * Traduz modelos de dados de um sistema para os padrões do Winker
 *
 * @package Winker\Integration\Util\Model
 */
class Translation {

    /**
     * Traduz um modelo de dado com as opções que foram passadas
     *
     * @param $data array|object Dados que devem ser traduzidos
     * @param $translate array Opções de tradução (ex: ['phone' => 'telefone_cond', 'name' => ['fantasia_cond', 'nome_cond']])
     * @param $model string|CustomModel Model que faz a tradução (ex: Portal::class)
     * @return array
     */
    public static function translate($data, array $translate, $model = null) {
        $allOpts    = self::generateOptions($translate);
        $return     = [];

        /**
         * Convert unique_id => original id
         */
        if (!empty($allOpts['unique_id']) && !empty($data['unique_id'])) {
            $parsed = UniqueId::parse($data['unique_id']);

            if (!empty($parsed[$allOpts['unique_id']['field_id_name']])) {
                $return[$allOpts['unique_id']['to']] = $parsed[$allOpts['unique_id']['field_id_name']];
            }
        }

        foreach ($allOpts['fields'] as $opt) {
            $from   = $opt[0];
            $to     = $opt[1];

            if (!empty($return[$to])) {
                continue;
            }

            if (strpos($to, 'unique_id') !== false) {
                continue;
            }

            $return[$to] = null;

            $value = null;

            if (strpos($from, '+') !== false) {
                $optsFrom = explode('+', $from);

                $fromValues = [];

                foreach ($optsFrom as $optFrom) {
                    if (!empty($data[$optFrom])) {
                        $fromValues[] = $data[$optFrom];
                    }
                }

                $value = implode(' ', $fromValues);
            } else if (strpos($from, '|') !== false) {
                $optsFrom = explode('|', $from);
                $value    = !empty($data[$optsFrom[0]]) ? $data[$optsFrom[0]] : $data[$optsFrom[1]];
            } else {
                if (!array_key_exists($from, $data)) {
                    continue;
                } else {
                    $value = is_array($data[$from]) ? $data[$from] : trim($data[$from]);
                }
            }

            $return[$to] = self::translateField($to, $value, $model);
        }

        if (array_key_exists('unique_id', $translate)) {
            $return = array_merge(['unique_id' => self::generateUniqueId($translate['unique_id'], $data)], $return);
        } else {
            if ($model && !is_object($model)) {
                self::attachUniqueId($return, $model);
            }
        }

        if ($model && !is_object($model)) {
            self::attachRelations($return, $model);
        }

        self::translateClosures($allOpts, $data, $return);
        self::translateCollection($allOpts, $data, $return);

        return $return;
    }

    private static function translateCollection($allOpts, $data, &$return) {
        foreach ($allOpts['collection'] as $field => $collection) {
            $key        = $collection['key'];
            $resolved   = (array) self::resolve($data, $key);

            if (empty($resolved)) {
                continue;
            }

            if (!empty($resolved[0])) {
                $rows = [];

                foreach ($resolved as $row) {
                    if(empty($row)) {
                        continue;
                    }
                    $rows[] = self::translate($row, $collection['subkeys']);
                }

                $return[$field] = $rows;
            } else {
                $return[$field] = self::translate($resolved, $collection['subkeys']);
            }
        }
    }

    private static function translateClosures($allOpts, $data, &$return) {
        foreach ($allOpts['closures'] as $field => $closure) {
            if ($closure instanceof OptionsTranslation) {
                $return[$field] = $closure->translateFromData($data);
            } else if ($closure instanceof DateTranslation) {
                $return[$field] = $closure->translateFromData($data);
            } else {
                $return[$field] = $closure($data);
            }
        }
    }

    private static function attachUniqueId(&$return, $model) {
        $uniqueIdFormat = $model::unique_id();

        if (empty($uniqueIdFormat)) {
            return;
        }

        $return = array_merge(['unique_id' => self::generateUniqueId($uniqueIdFormat, $return)], $return);
    }

    private static function attachRelations(&$return, $model) {
        $relationsFormat = $model::relations();

        if (empty($relationsFormat)) {
            return;
        }

        $return['relations'] = [];

        foreach ($relationsFormat as $rel_model => $rel_unique_id) {
            $return['relations'][$rel_model] = self::generateUniqueId($rel_unique_id, $return);
        }
    }

    private static function generateUniqueId($opts, $data) {
        $ids = [];

        foreach ($opts as $modelUID => $value) {
            if (!array_key_exists($value, $data)) {
                throw new Exception("unique_id need \"{$value}\" field.");
            }

            $ids[] = "{$modelUID}:".trim($data[$value]);
        }

        return implode('|', $ids);
    }

    /**
     * Traduz uma lista de modelos
     *
     * @param array $data
     * @param $opts
     * @param $model|CustomModel
     * @return array
     */
    public static function translateList(array $data, $translate, $model = null) {
        $return = [];

        foreach ($data as $row) {
            $return[] = self::translate($row, $translate, $model);
        }

        return $return;
    }

    private static function generateOptions($translate) {
        $allOpts = [
            'fields'        => [],
            'closures'      => [],
            'unique_id'     => [],
            'collection'    => [],
        ];

        foreach ($translate as $to => $from) {
            if (is_array($from)) {
                foreach ($from as $from2Key => $from2) {
                    if ($from2 === 'unique_id') {
                        $allOpts['unique_id'] = ['to' => $to, 'field_id_name' => $from[1]];

                        break;
                    }

                    if (is_int($from2Key)) {
                        $allOpts['fields'][] = [$from2, $to];
                    } else if ($to !== 'unique_id') {
                        $allOpts['collection'][$to] = $from;
                    }
                }
            } else if (is_object($from)) {
                $allOpts['closures'][$to] = $from;
            } else {
                $allOpts['fields'][] = [$from, $to];
            }
        }

        return $allOpts;
    }

    private static function translateField($field, $value, $model = null) {
        if (empty($value)) {
            return $value;
        }

        if (empty($model)) {
            return $value;
        }

        if (is_object($model)) {
            return $model->translateFieldCustomModel($field, $value);
        } else {
            return $model::translateField($field, $value);
        }
    }

    private static function resolve(array $a, $path, $default = null) {
        $current = $a;
        $p = strtok($path, '.');

        while ($p !== false) {
            if (!isset($current[$p])) {
                return $default;
            }
            $current = $current[$p];
            $p = strtok('.');
        }

        return $current;
    }
}
