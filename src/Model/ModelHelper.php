<?php
namespace Winker\Integration\Util\Model;

class ModelHelper {
    public static function extractUniqueIds($models) {
        $uniquesIds = [];

        foreach ($models as $model) {
            $uniquesIds[] = $model['unique_id'];
        }

        return $uniquesIds;
    }
}