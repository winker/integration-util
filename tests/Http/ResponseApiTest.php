<?php

use Winker\Integration\Util\Http\Exception\RequestException;
use Winker\Integration\Util\Http\ResponseApi;

class ResponseApiTest extends PHPUnit_Framework_TestCase {

    public function testSuccess() {

        $expected = [
            'success' => true
        ];

        $actual = ResponseApi::success();

        $this->assertArraySubset($expected, $actual);

        $expected = [
            'success' => true,
            'data'    => [
                1, 2, 3
            ]
        ];

        $actual = ResponseApi::success(
            [
                1, 2, 3
            ]
        );

        $this->assertArraySubset($expected, $actual);
    }

    public function testErrorNullString() {

        $expected = [
            'success' => false
        ];

        $actual = ResponseApi::error();

        $this->assertArraySubset($expected, $actual);
    }

    public function testErrorStringArray() {

        $expected = [
            'success' => false,
            'errors'  => [
                'Error message.'
            ]
        ];

        $actual = ResponseApi::error('Error message.');

        $this->assertArraySubset($expected, $actual);
    }

    public function testErrorStringArrayTrace() {

        $expected = [
            'success' => false,
            'trace'   => 'Error message: 1.',
            'errors'  => 'Error message: 2.'
        ];

        $actual = ResponseApi::error(
            [
                'trace'   => 'Error message: 1.',
                'message' => 'Error message: 2.'
            ]
        );

        $this->assertArraySubset($expected, $actual);
    }

    public function testErrorStringExecption() {

        $actual = ResponseApi::error(new Exception('Error message: 1.'));
        $expected = [
            'success' => false,
            'errors'  => ['Error message: 1.'],
        ];
        unset($actual['trace']);
        $this->assertArraySubset($expected, $actual);

    }

    public function testErrorStringRequestExecption() {

        $actual = ResponseApi::error(new RequestException('Error message: 1.'));
        $expected = [
            'success' => false,
            'errors'  => 'Error message: 1.',
        ];
        unset($actual['trace']);
        $this->assertArraySubset($expected, $actual);

    }

    public function testErrorStringInvalidObject() {

        $obj = new stdClass();
        $obj->message = 'teste';
        $actual       = ResponseApi::error($obj);

        $expected = [
            'success' => false,
            'errors'  => ['Inválid Error'],
        ];

        $this->assertArraySubset($expected, $actual);

    }

    public function testFromMultiplesResponses() {

        $responses = [
            [
                'success' => 1,
                'data'    => [
                    [
                        'queue_added' => 1,
                        'unique_id'   => 'Portal:1',
                        'protocol'    => 'prot-1',
                    ],
                    [
                        'queue_added' => 1,
                        'unique_id'   => 'Portal:2',
                        'protocol'    => 'prot-2',
                    ],
                    [
                        'queue_added' => 0,
                        'unique_id'   => 'Portal:3',
                        'errors'      => ['Some error happened.'],
                    ]
                ]
            ],
            [
                'success' => 1,
                'data'    => [
                    [
                        'queue_added' => 1,
                        'unique_id'   => 'Portal:4',
                        'protocol'    => 'prot-4',
                    ],
                ]
            ],
            [
                'success' => 0,
                'errors'  => ['Other X error happened.']
            ],
            [
                'success' => 0,
                'errors'  => ['Other Y error happened.']
            ],
            [
                'success' => 0,
                'errors'  => ['Other Z error happened.']
            ],
            [
                'success' => 0,
                'errors'  => ['field_name' => 'Error happened.']
            ],


            [
                'success' => 0,
                'errors'  => ['field_name2' => ['Error happened.']]
            ]
        ];

        $this->assertArraySubset([
            'success' => 1,
            'data'    => [
                [
                    'queue_added' => 1,
                    'unique_id'   => 'Portal:1',
                    'protocol'    => 'prot-1',
                ],
                [
                    'queue_added' => 1,
                    'unique_id'   => 'Portal:2',
                    'protocol'    => 'prot-2',
                ],
                [
                    'queue_added' => 0,
                    'unique_id'   => 'Portal:3',
                    'errors'      => ['Some error happened.'],
                ],
                [
                    'queue_added' => 1,
                    'unique_id'   => 'Portal:4',
                    'protocol'    => 'prot-4',
                ]
            ],
            'errors' => [
                'Other X error happened.',
                'Other Y error happened.',
                'Other Z error happened.',
                'field_name'  => 'Error happened.',
                'field_name2' => ['Error happened.'],
            ]
        ], ResponseApi::fromMultiplesResponses($responses));
    }
}