<?php

use \Winker\Integration\Util\Http\UrlHelper;

class UrlHelperTest extends TestCase {
    public function testParseQuery() {
        $this->assertArraySubset(['param' => 'value'], UrlHelper::parse_query('param=value'));
        $this->assertArraySubset(['param' => 'value', 'param2' => 'value2'], UrlHelper::parse_query('param=value&param2=value2'));
        $this->assertArraySubset(['param' => 'value', 'param2' => ''], UrlHelper::parse_query('param=value&param2='));
        $this->assertArraySubset([], UrlHelper::parse_query(''));
    }

    public function testUnParseQuery() {
        $this->assertEquals('param=value', UrlHelper::unparse_query(['param' => 'value']));
        $this->assertEquals('param=value&param2=value2', UrlHelper::unparse_query(['param' => 'value', 'param2' => 'value2']));
        $this->assertEquals('param=value&param2=', UrlHelper::unparse_query(['param' => 'value', 'param2' => '']));
        $this->assertEquals('', UrlHelper::unparse_query([]));
    }

    public function testBuild() {
        $this->assertEquals('controller/action?id_portal=35&page=2', UrlHelper::build('controller/action?id_portal=35&page=1', ['page' => 2]));
        $this->assertEquals('controller/action?page=2&id_portal=35', UrlHelper::build('controller/action?page=1&id_portal=35', ['page' => 2]));
        $this->assertEquals('controller/action?page=2', UrlHelper::build('controller/action?page=1', ['page' => 2]));
        $this->assertEquals('controller/action?page=2', UrlHelper::build('controller/action', ['page' => 2]));
        $this->assertEquals('controller/action', UrlHelper::build('controller/action', []));
    }
}