<?php

use \Winker\Integration\Util\Http\Client;

class ClientSoapTest extends PHPUnit_Framework_TestCase {
    public function testCall() {
        $data = [
            'Sums'              => 6,
            'Average'           => 2,
            'StandardDeviation' => 0.81649658092773,
            'skewness'          => 0.61237243569579,
            'Kurtosis'          => -2.25,
        ];

        $client = new Client();
        $client->setWsdl('http://www.webservicex.net/Statistics.asmx?WSDL');
        $client->mockResponses([$data]);

        $this->assertArraySubset($data, $client->call('GetStatistics', [['X' => [1,2,3]]]));
    }

    public function testAuth() {
        $data = [
            'result' => [
                'nome' => 'ROSA AMELIA SALGUEIRO DE MELO'
            ]
        ];

        $client = new Client();
        $client->setWsdl('https://sitenethomologa.serasa.com.br:443/experian-data-infobusca-ws/dataLicensingService?wsdl', 'username', 'password');
        $client->mockResponses([$data]);

        $response = $client->call('ConsultarPF', [[
            'parameters' => [
                'cpf' => "64872092368",
                'RetornoPF' => [
                    'nome'  => true
                ],
            ]
        ]]);

        $this->assertArraySubset($data, $response);
    }
}