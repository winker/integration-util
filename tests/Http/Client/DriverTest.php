<?php

use \Winker\Integration\Util\Http\Client;
use \Winker\Integration\Util\Http\Client\Driver;

class DriverTest extends PHPUnit_Framework_TestCase {
    /**
     * @var Client
     */
    private $client;

    public function setUp() {
        $this->client = new Client();
        $this->client->setDriver(new MyDriver());
    }

    public function testDriver() {
        $this->client->mockResponse('response ok');
        $response = $this->client->get('/dogs');

        $this->assertEquals('response ok', $response);
    }

    public function testGetPageParamName() {
        $this->assertEquals('page', $this->client->getDriver()->getPageParamName());
    }

    public function testGetMethodDefault() {
        $this->assertEquals('GET', $this->client->getDriver()->getMethodDefault());
    }

    public function testGetWhileNextPageResponseCheck() {
        $this->assertTrue($this->client->getDriver()->getWhileNextPageResponseCheck() instanceof Closure);
    }
}