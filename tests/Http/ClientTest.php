<?php

use \Winker\Integration\Util\Http\Client;

class ClientTest extends PHPUnit_Framework_TestCase {
    public function testPost() {
        $data = ['status' => 'ok', 'content' => [1, 2, 3]];

        $client = new Client();
        $client->mockResponses([$data]);
        $response = $client->post('/dog/put', $data);

        $this->assertArraySubset($data, $response);
    }

    public function testGet() {
        $data = ['status' => 'ok', 'data' => ['Dog 1', 'Dog 2', 'Dog 3']];

        $client = new Client();
        $client->mockResponses([$data]);
        $response = $client->get('/dogs');

        $this->assertArraySubset($data, $response);
    }

    public function testException() {
        $client = new Client();
        $client->mockResponses([new \GuzzleHttp\Exception\RequestException("Error Communicating with Server", new \GuzzleHttp\Psr7\Request('GET', 'test'))]);

        $errors = [];

        try {
            $response = $client->get('/test');
        } catch (\Winker\Integration\Util\Http\Exception\RequestException $e) {
            $errors = json_decode($e->getMessage());
        }

        $this->assertArraySubset(['Error Communicating with Server'], $errors);
    }

    public function testStringResponses() {
        $client = new Client();
        $client->mockResponses(['Dogs not found']);
        $response = $client->get('/dogs');

        $this->assertEquals('Dogs not found', $response);
    }

    public function testStringResponse() {
        $client = new Client();
        $client->mockResponse('Dogs not found');
        $response = $client->get('/dogs');

        $this->assertEquals('Dogs not found', $response);
    }

    public function testRawTest() {
        putenv("WINKER_INTEGRATION_API_URL=https://mqlinuxext.serasa.com.br/Homologa");

        $data = [
            'result' => 'B49C      000000000000353FC     FI0000200            N99SFIMAN                            SS            00S        000000000000000S         1                          000000000               00S 2017031010375500360038    0039                                                                        0000                    3#                                  00                                      N  P006SSSSS  99SSSSSSNS NNNNNS                                                                                       B001ATILIA OLIVEIRA                              0000000035300000000000000019610101SPO SN   392981604220160928   2CB002   2016092919610101DNA ATILIA                                   F               000000000000000     00000000  CB003            00                                       0000000000000113257374900000000000000000000000000        CB004R DA MOOCA                    1000           MOOCA               SAO PAULO                SP03104010000000    CB3530902017030140250260250030000000010022016092900000000                                                          CB35420170310ASSECAD BRASIL ASSESSORIA CADASTRAL LTDA               00000000007517328                              CB35420170310REDE DOMUS                                             00000000018924102                              CB35420170309CIA BRAS MEIOS DE PAGAMENTO                            00000000001027058                              CB35420170309CREDIFAR S/A CRED FINANCIAMENTO INVESTIM               00000000005676026                              CB35420170309REDE DOMUS                                             00000000018924102                              CB3701000000000000R BAUCIS                      14                                      JD S LUIS           05844030B3702SAO PAULO                     SP                                                  20160921            00000000B3701011032573749R MARTINS FONTES              164  AP 6606                            CENTRO              01050000B3702SAO PAULO                     SPMANOEL AMORIM REGO                                20170310            00000000B35700001PENDENCIA FINANCEIRA        201606201606R$ 000001000????????????????????    01000001000                  CB35801ADIANT CONTAV0001842509  20160626ADNR$ 0000010008888954          ????????????????????    00001       AD A   CB352ICE PLACE SORVETERIA E LANCHES LTDA     0002740600070SPSITUACAO DO CNPJ EM 31/12/2010: ATIVA      201607201607CB001PAULO RIBEIRO                                0000000035300000000000000000000000SPO      200000002220160928   2CB002   2016092119000101ISELINA COSME RIBEIRO                        M               000000000000000     00000000  CB003            00                                       0000000000000000000000000000000000000008356910911        CB004R BAUCIS                      14             JD SAO LUIS         SAO PAULO                SP05844030190001    CB001IVO DE SOUZA FERREIRA                        0000000035300000000000000000000000         553732351220160928   2CB002   2005031819860104                                             F               000000000000000     00000000  CB003            00                                       0000000000000000000000000000000000000000000000000        CB001ENIVALDO ABADIA ANTUNES                      0000000035300000000000000000000000         557725182220160928   2CB002   2005072519720604                                             M               000000000000000     00000000  CB003            00                                       0000000000000000000000000000000000000000000000000        CB001CLIENTE TESTE FLAG INTEGRAR                  0000000035300000000000000000000000         565203737220160928   2CB002   2006021719700101                                             F               000000000000000     00000000  CB003            00                                       0000000000000000000000000000000000000000000000000        CB001MARIA SUELI DA SILVA SANTOS                  0000000035300000000000000000000000         572664504220160928   2CB002   2006062619690511                                             F               000000000000000     00000000  CB003            00                                       0000000000000000000000000000000000000000000000000        CB001THIAGO TESTE CPAC                            0000000035300000000000000000000000         588116870220160928   2CB002   2007061919700101                                             F               000000000000000     00000000  CB003            00                                       0000000000000000000000000000000000000000000000000        CT999000PROCESSO ENCERRADO NORMALMENTE'
        ];

        $params = [
            'p' => "0103512210203040        B49C      000000000000353FC     FI0000000            N99SINIAN                                          00S        000000000000000S         1                                                                                                                                                                                                                                                                   P006SSSSS  99SSSSSSNS NNNNNS                                                                                       T999"
        ];

        $client = new Client();
        $client->mockResponses([$data]);

        $driver = new \Winker\Integration\Util\Integration\Api\Driver();
        $driver->setType("RAW");
        $client->setDriver($driver);

        $response = (array)json_decode(trim(preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $client->post('consultahttps', $params))));

        $this->assertEquals($data["result"], $response["result"]);
    }
}