<?php
class TestCase extends PHPUnit_Framework_TestCase {
    public function mockRequest($response) {
        $client = app(\Winker\Integration\Util\Http\Client::class);

        $responses = [];

        for ($i = 0; $i <= 99; $i++) {
            $responses[] = json_decode($response, true);
        }

        $client->mockResponses($responses);
    }
}

class MyDriver extends \Winker\Integration\Util\Http\Client\Driver {
    public function getBaseUri() {
        return 'http://localhost/';
    }
}

class PortalTranslator extends \Winker\Integration\Util\Model\TranslatorDefinition {

    public function winkerModelTranslation() {
        return \Winker\Integration\Util\Model\Translation\Model\Portal::class;
    }

    public function winkerFieldsTranslation() {
        return [
            'unique_id'             => ['Portal' => 'id_condominio_cond'],
            'name'                  => ['st_fantasia_cond', 'st_nome_cond'],
            'doc_cnpj'              => 'st_cpf_cond',
            'phone'                 => 'st_telefone_cond',
            'address'                => 'st_endereco_cond',
            'address_complement'     => 'st_complemento_cond',
            'address_neighborhood'   => 'st_bairro_cond',
            'address_city'           => 'st_cidade_cond',
            'address_state'          => 'st_estado_cond',
            'address_zipcode'        => 'st_cep_cond'
        ];
    }

    public function vendorModelTranslation() {
        return null;
    }

    public function vendorFieldsTranslation() {
        return [
            'id_condominio_cond'    => ['unique_id', 'Sub'],
            'st_fantasia_cond'      => 'name',
            'st_cpf_cond'           => 'cnpj',
            'st_telefone_cond'      => 'phone',
            'st_endereco_cond'      => 'address',
            'st_complemento_cond'   => 'address_complement',
            'st_bairro_cond'        => 'address_neighborhood',
            'st_cidade_cond'        => 'address_city',
            'st_estado_cond'        => 'address_state',
            'st_cep_cond'           => 'address_zipcode'
        ];
    }
}