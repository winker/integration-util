<?php

class ApiTest extends TestCase {
    public function testAddQueue_true() {
        $api = new \Winker\Integration\Util\Integration\Api();
        $api->mockJsonResponse('{"success":true,"data":{"queue_added":true,"unique_id":"Portal#31|Division#10","protocol":"440f7055-d0f8-454a-8f7e-4e76ad82944a"}}');

        $response = $api->addQueue('/v1/division', ['Portal' => 31, 'Division' => 10], [
            'name' => 'Division A',
        ]);

        $this->assertArraySubset([
            'success'   => true,
            'data'      => [
                'queue_added'   => true,
                'unique_id'     => 'Portal#31|Division#10',
                'protocol'      => '440f7055-d0f8-454a-8f7e-4e76ad82944a',
            ],
        ], $response);
    }

    public function testRequestQueueAdded_post_true() {
        $api = new \Winker\Integration\Util\Integration\Api();
        $api->mockJsonResponse('{"success":true,"data":{"queue_added":true,"unique_id":"Portal#31","protocol":"440f7055-d0f8-454a-8f7e-4e76ad82944a"}}');

        $response = $api->post('/v1/portal', [
            'name'                  => 'Ilhas Belas Integrado',
            'doc_cnpj'              => '38.941.510/0001-36',
            'address'                => 'Rua de Teste',
            'address_complement'     => 'Complement',
            'address_neighborhood'   => 'Nome do Bairro',
            'address_city'           => 'Nome da Cidade',
            'address_state'          => 'SC',
            'address_zipcode'        => '88123456',
            'unique_id'             => 'Portal#31',
        ]);

        $this->assertArraySubset([
            'success'   => true,
            'data'      => [
                'queue_added'   => true,
                'unique_id'     => 'Portal#31',
                'protocol'      => '440f7055-d0f8-454a-8f7e-4e76ad82944a',
            ],
        ], $response);
    }

    public function testRequestQueueAdded_post_false() {
        $api = new \Winker\Integration\Util\Integration\Api();
        $api->mockJsonResponse('{"success":true,"data":{"queue_added":false,"unique_id":"Portal#25","errors":["Integration is processing. Try again later."]}}');

        $response   = $api->post('/v1/portal', [
            'name'                  => 'Ilhas Belas Integrado',
            'doc_cnpj'              => '38.941.510/0001-36',
            'address'                => 'Rua de Teste',
            'address_complement'     => 'Complement',
            'address_neighborhood'   => 'Nome do Bairro',
            'address_city'           => 'Nome da Cidade',
            'address_state'          => 'SC',
            'address_zipcode'        => '88123456',
            'unique_id'             => 'Portal#25',
        ]);

        $this->assertArraySubset([
            'success'   => true,
            'data'      => [
                'queue_added'   => false,
                'unique_id'     => 'Portal#25',
                'errors'        => [
                    'Integration is processing. Try again later.'
                ],
            ],
        ], $response);
    }
}