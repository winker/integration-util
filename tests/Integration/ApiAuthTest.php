<?php

use \Winker\Integration\Util\Integration\Api;

class ApiAuthTest extends TestCase {

    /**
     * @var Api
     */
    private $api;

    public function setUp() {
        $this->api = new Api();
        $this->api->getCache()->clear();
    }

    public function testAuth_success() {
        $token = getenv('WINKER_INTEGRATION_API_TOKEN');

        $this->api->mockJsonResponse('{"success":true,"data":{"id_user_integration":100,"manager":{"id_manager":50,"name":"Manager Name"}}}');
        $auth = $this->api->auth($token);

        $this->assertArraySubset([
            'success'   => true,
            'data'      => [
                'id_user_integration'   => 100,
                'manager'               => [
                    'id_manager'    => 50,
                    'name'          => 'Manager Name',
                ],
            ],
        ], $auth);
    }

    public function testAuth_falied() {
        $this->api->mockJsonResponse('{"success":false,"errors":["User not found."]}');
        $auth = $this->api->auth('invalid-token');

        $this->assertArraySubset([
            'success'   => false,
            'errors'    => ['User not found.'],
        ], $auth);
    }

    public function testAuth_without_token() {
        $this->api->mockJsonResponse('{"success":false,"errors":["Token not found."]}');
        $auth = $this->api->auth(null);

        $this->assertArraySubset([
            'success'   => false,
            'errors'    => ['Token not found.'],
        ], $auth);
    }

    public function testAuthFromRequest_from_get() {
        $_GET['__token'] = getenv('WINKER_INTEGRATION_API_TOKEN');

        $this->api->mockJsonResponse('{"success":true,"data":{"id_user_integration":100,"manager":{"id_manager":50,"name":"Manager Name"}}}');
        $auth = $this->api->authFromRequest();

        $this->assertArraySubset([
            'success' => true,
            'data' => [
                'id_user_integration' => 100,
                'manager' => [
                    'id_manager' => 50,
                    'name' => 'Manager Name',
                ],
            ],
        ], $auth);
    }

    public function testAuthFromRequest_from_header() {
        $_SERVER['HTTP_X_AUTH_TOKEN'] = getenv('WINKER_INTEGRATION_API_TOKEN');

        $this->api->mockJsonResponse('{"success":true,"data":{"id_user_integration":100,"manager":{"id_manager":50,"name":"Manager Name"}}}');
        $auth = $this->api->authFromRequest();

        $this->assertArraySubset([
            'success'   => true,
            'data'      => [
                'id_user_integration'   => 100,
                'manager'               => [
                    'id_manager'    => 50,
                    'name'          => 'Manager Name',
                ],
            ],
        ], $auth);
    }
}