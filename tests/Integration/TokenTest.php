<?php

use \Winker\Integration\Util\Integration\Token;

class TokenTest extends TestCase {
    public function testDiscovery_env() {
        $this->assertEquals('user_integration_token', Token::discovery());

        $_SERVER['WINKER_INTEGRATION_API_TOKEN'] = null;
        $this->assertNull(Token::discovery());
    }

    public function testDiscovery_get() {
        $_SERVER['WINKER_INTEGRATION_API_TOKEN'] = null;
        $_GET['__token'] = 'user_integration_token_2';

        $this->assertEquals('user_integration_token_2', Token::discovery());
    }

    public function testDiscovery_auth_token() {
        $_SERVER['WINKER_INTEGRATION_API_TOKEN'] = null;
        $_SERVER['HTTP_X_AUTH_TOKEN'] = 'user_integration_token_3';

        $this->assertEquals('user_integration_token_3', Token::discovery());
    }
}