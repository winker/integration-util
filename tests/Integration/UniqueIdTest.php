<?php
use \Winker\Integration\Util\Integration\UniqueId;

class UniqueIdTest extends TestCase {
    public function testParse() {
        $this->assertArraySubset(['Portal' => 123], UniqueId::parse('Portal:123'));
        $this->assertArraySubset(['Portal' => 123, 'SUB1' => 777], UniqueId::parse('Portal:123|SUB1:777'));
        $this->assertArraySubset(['Portal' => 123, 'SUB1' => 777, 'SUB2' => 888], UniqueId::parse('Portal:123|SUB1:777|SUB2:888'));
        $this->assertArraySubset([], UniqueId::parse(''));
        $this->assertArraySubset([], UniqueId::parse(null));
    }
}