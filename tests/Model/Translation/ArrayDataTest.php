<?php

use \Winker\Integration\Util\Model\Translation;
use \Winker\Integration\Util\Model\Translation\Model\Portal;

class ArrayDataTest extends PHPUnit_Framework_TestCase {

    protected function setUp() {
        $this->exampleData = [
            'id_condominio'         => '123',
            'fantasia_cond'         => 'Ilhas Belas',
            'situacao_cadastral'    => [
                'nome'              => 'ROSA AMELIA SALGUEIRO DE MELO',
                'situacao'          => 'ATIVO',
                'data_consulta'     => '2011-07-31T00:00:00.000-03:00'
            ]
        ];

        $this->exampleTranslate = [
            'unique_id'             => ['Portal' => 'id_condominio'],
            'name'                  => ['fantasia_cond', 'nome_cond'],
            'legal_situation'       => [
                'key'       => 'situacao_cadastral',
                'subkeys'   => [
                    'name'          => 'nome',
                    'situation'     => 'situacao',
                    'query_date'    => 'data_consulta'
                ]
            ],
        ];
    }

    public function testTranslate() {
        $translated = Translation::translate($this->exampleData, $this->exampleTranslate, Portal::class);

        $this->assertArraySubset([
            'unique_id'             => 'Portal:123',
            'name'                  => 'Ilhas Belas',
            'legal_situation'       => [
                'name'          => 'ROSA AMELIA SALGUEIRO DE MELO',
                'situation'     => 'ATIVO',
                'query_date'    => '2011-07-31T00:00:00.000-03:00'
            ]
        ], $translated);
    }

    public function testTranslateMultiArray() {
        $exampleData = [
            'id_condominio'         => '123',
            'fantasia_cond'         => 'Ilhas Belas',
            'situacao_cadastral'    => [
                'nome'              => 'ROSA AMELIA SALGUEIRO DE MELO',
                'situacao'          => 'ATIVO',
                'data_consulta'     => '2011-07-31T00:00:00.000-03:00',
                'bolsa_familia'     => [
                    'situacao'  =>  'ok'
                ]
            ]
        ];

        $exampleTranslate = [
            'unique_id'             => ['Portal' => 'id_condominio'],
            'name'                  => ['fantasia_cond', 'nome_cond'],
            'legal_situation'       => [
                'key'       => 'situacao_cadastral',
                'subkeys'   => [
                    'name'          => 'nome',
                    'situation'     => 'situacao',
                    'query_date'    => 'data_consulta',
                    'help_family'   => [
                        'key'       => 'bolsa_familia',
                        'subkeys'   => [
                            'situation' => 'situacao'
                        ]
                    ]
                ]
            ],
        ];


        $translated = Translation::translate($exampleData, $exampleTranslate, Portal::class);

        $this->assertArraySubset([
            'unique_id'             => 'Portal:123',
            'name'                  => 'Ilhas Belas',
            'legal_situation'       => [
                'name'          => 'ROSA AMELIA SALGUEIRO DE MELO',
                'situation'     => 'ATIVO',
                'query_date'    => '2011-07-31T00:00:00.000-03:00',
                'help_family'   => [
                    'situation' => 'ok'
                ]
            ]
        ], $translated);
    }

    public function testTranslateValues() {
        $exampleData = [
            'id_condominio'         => '123',
            'fantasia_cond'         => 'Ilhas Belas',
            'situacao_cadastral'    => [
                'nome'              => 'ROSA AMELIA SALGUEIRO DE MELO',
                'situacao'          => 'ATIVO',
                'data_consulta'     => '12/06/2011 10:25:30'
            ]
        ];

        $exampleTranslate = [
            'unique_id'             => ['Portal' => 'id_condominio'],
            'name'                  => ['fantasia_cond', 'nome_cond'],
            'legal_situation'       => [
                'key'       => 'situacao_cadastral',
                'subkeys'   => [
                    'name'          => 'nome',
                    'situation'     => new Translation\OptionsTranslation(['ATIVO' => 'active'], 'situacao'),
                    'query_date'    => new Translation\DateTranslation(['d/m/Y' => DateTime::ISO8601], 'data_consulta')
                ]
            ],
        ];


        $translated = Translation::translate($exampleData, $exampleTranslate, Portal::class);

        $this->assertArraySubset([
            'unique_id'             => 'Portal:123',
            'name'                  => 'Ilhas Belas',
            'legal_situation'       => [
                'name'          => 'ROSA AMELIA SALGUEIRO DE MELO',
                'situation'     => 'active',
                'query_date'    => '2011-06-12T10:25:30+0000'
            ]
        ], $translated);
    }

    public function testTranslateWithOutKey() {
        $exampleData = [
            'id_condominio' => '123',
            'fantasia_cond'  => 'Ilhas Belas',
        ];

        $translated = Translation::translate($exampleData, $this->exampleTranslate, Portal::class);

        $this->assertArraySubset([
            'unique_id' => 'Portal:123',
            'name'      => 'Ilhas Belas'
        ], $translated);
    }

    public function testTranslateArraysInsideOneArray() {
        $exampleData = [
            'id_condominio'         => '123',
            'fantasia_cond'         => 'Ilhas Belas',
            'enderecos'             => [
                'endereco' => [
                    ['logradouro' => 'Rua A', 'numero' => 123],
                    ['logradouro' => 'Rua B', 'numero' => 456],
                ]
            ]
        ];

        $exampleTranslate = [
            'unique_id'     => ['Portal' => 'id_condominio'],
            'name'          => ['fantasia_cond', 'nome_cond'],
            'addresses'     => [
                'key'       => 'enderecos.endereco',
                'subkeys'   => [
                    'address'   => 'logradouro',
                    'number'    => 'numero',
                ]
            ],
        ];

        $translated = Translation::translate($exampleData, $exampleTranslate, Portal::class);

        $this->assertArraySubset([
            'unique_id' => 'Portal:123',
            'name'      => 'Ilhas Belas',
            'addresses' => [
                ['address' => 'Rua A', 'number' => 123],
                ['address' => 'Rua B', 'number' => 456],
            ]
        ], $translated);
    }

    public function testTranslateArrayEmptyData() {
        $exampleData = [
            'id_condominio'         => '123',
            'fantasia_cond'         => 'Ilhas Belas',
            'enderecos'             => [
                'endereco' => [
                    ['logradouro' => 'Rua A', 'numero' => 123],
                    ['logradouro' => 'Rua B', 'numero' => 456],
                    [],
                ]
            ]
        ];

        $exampleTranslate = [
            'unique_id'     => ['Portal' => 'id_condominio'],
            'name'          => ['fantasia_cond', 'nome_cond'],
            'addresses'     => [
                'key'       => 'enderecos.endereco',
                'subkeys'   => [
                    'address'   => 'logradouro',
                    'number'    => 'numero',
                ]
            ],
        ];

        $translated = Translation::translate($exampleData, $exampleTranslate, Portal::class);

        $this->assertEquals(2, count($translated['addresses']));
    }
    
    public function testTranslateArrayNumericIndex() {
        $exampleData = [
            'id_condominio'         => '123',
            'fantasia_cond'         => 'Ilhas Belas',
            'enderecos'             => [
                    ['logradouro' => 'Rua A', 'numero' => 123],
                    ['logradouro' => 'Rua B', 'numero' => 456],
            ]
        ];

        $exampleTranslate = [
            'unique_id'     => ['Portal' => 'id_condominio'],
            'name'          => ['fantasia_cond', 'nome_cond'],
            'addresses'     => [
                'key'       => 'enderecos',
                'subkeys'   => [
                    'address'   => 'logradouro',
                    'number'    => 'numero',
                ]
            ],
        ];

        $translated = Translation::translate($exampleData, $exampleTranslate, Portal::class);

        $this->assertArraySubset([
            'unique_id' => 'Portal:123',
            'name'      => 'Ilhas Belas',
            'addresses' => [
                ['address' => 'Rua A', 'number' => 123],
                ['address' => 'Rua B', 'number' => 456],
            ]
        ], $translated);
    }    
}