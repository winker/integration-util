<?php

use \Winker\Integration\Util\Model\Translation;
use \Winker\Integration\Util\Model\Translation\Field\CNPJ;

class CNPJTest extends PHPUnit_Framework_TestCase {

    public function testTranslate() {
        $this->assertEquals('93329359000198', CNPJ::translate('93.329.359/0001-98'));
        $this->assertEquals('93329359000198', CNPJ::translate('93329359000198'));
    }
}