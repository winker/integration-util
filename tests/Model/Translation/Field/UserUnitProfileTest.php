<?php

use \Winker\Integration\Util\Model\Translation;
use \Winker\Integration\Util\Model\Translation\Field\UserUnitProfile;

class UserUnitProfileTest extends PHPUnit_Framework_TestCase {

    public function testTranslate() {
        $this->assertEquals('Owner', UserUnitProfile::translate('Proprietário'));
        $this->assertEquals('Owner', UserUnitProfile::translate('Proprietario'));
        $this->assertEquals('Owner', UserUnitProfile::translate('proprietario'));
        $this->assertEquals('Owner', UserUnitProfile::translate('Dono'));
        $this->assertEquals('Owner', UserUnitProfile::translate('dono'));
        $this->assertEquals('Tenant', UserUnitProfile::translate('Inquilino'));
        $this->assertEquals('Tenant', UserUnitProfile::translate('inquilino'));
        $this->assertEquals('Tenant', UserUnitProfile::translate('temporário'));
        $this->assertEquals('Tenant', UserUnitProfile::translate('temporario'));
        $this->assertEquals('', UserUnitProfile::translate(''));
        $this->assertEquals('', UserUnitProfile::translate(null));
    }
}