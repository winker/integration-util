<?php

use \Winker\Integration\Util\Model\Translation;
use \Winker\Integration\Util\Model\Translation\Field\CPF;

class CPFTest extends PHPUnit_Framework_TestCase {

    public function testTranslate() {
        $this->assertEquals('36320143452', CPF::translate('363.201.434-52'));
        $this->assertEquals('36320143452', CPF::translate('36320143452'));
    }
}