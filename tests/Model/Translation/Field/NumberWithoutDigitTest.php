<?php

use \Winker\Integration\Util\Model\Translation;
use \Winker\Integration\Util\Model\Translation\Field\NumberWithoutDigit;

class NumberWithoutDigitTest extends PHPUnit_Framework_TestCase {

    public function testTranslate() {
        $this->assertEquals('', NumberWithoutDigit::translate(''));
        $this->assertEquals('1', NumberWithoutDigit::translate('1'));
        $this->assertEquals('1234', NumberWithoutDigit::translate('1234'));
        $this->assertEquals('1234', NumberWithoutDigit::translate('1234-5'));
        $this->assertEquals('1234', NumberWithoutDigit::translate('1234--5'));
        $this->assertEquals('1234', NumberWithoutDigit::translate('1234 5'));
        $this->assertEquals('1234', NumberWithoutDigit::translate('1234  5'));
        $this->assertEquals('1234', NumberWithoutDigit::translate('1234 - 5'));
        $this->assertEquals('1234', NumberWithoutDigit::translate('1234:5'));
        $this->assertEquals('1234', NumberWithoutDigit::translate('1234_5'));
        $this->assertEquals('1234', NumberWithoutDigit::translate('1234/5'));
        $this->assertEquals('1234', NumberWithoutDigit::translate('1234#5'));
        $this->assertEquals('1234', NumberWithoutDigit::translate('1234@5'));
    }
}