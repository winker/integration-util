<?php

use \Winker\Integration\Util\Model\Translation;
use \Winker\Integration\Util\Model\Translation\Field\NumberDigit;

class NumberDigitTest extends PHPUnit_Framework_TestCase {

    public function testTranslate() {
        $this->assertEquals('', NumberDigit::translate(''));
        $this->assertEquals('', NumberDigit::translate('1'));
        $this->assertEquals('', NumberDigit::translate('1234'));
        $this->assertEquals('5', NumberDigit::translate('1234-5'));
        $this->assertEquals('5', NumberDigit::translate('1234--5'));
        $this->assertEquals('5', NumberDigit::translate('1234 5'));
        $this->assertEquals('5', NumberDigit::translate('1234  5'));
        $this->assertEquals('5', NumberDigit::translate('1234 - 5'));
        $this->assertEquals('5', NumberDigit::translate('1234:5'));
        $this->assertEquals('5', NumberDigit::translate('1234_5'));
        $this->assertEquals('5', NumberDigit::translate('1234/5'));
        $this->assertEquals('5', NumberDigit::translate('1234#5'));
        $this->assertEquals('5', NumberDigit::translate('1234@5'));
    }
}