<?php

use \Winker\Integration\Util\Model\Translation;
use \Winker\Integration\Util\Model\Translation\Field\State;

class StateTest extends PHPUnit_Framework_TestCase {

    public function testTranslate() {
        $this->assertEquals('SC', State::translate(42));
        $this->assertEquals('SC', State::translate('42'));
        $this->assertEquals('SC', State::translate('santa catarina'));
        $this->assertEquals('SC', State::translate('Santa Catarina'));
        $this->assertEquals('SC', State::translate('SANTA CATARINA'));
        $this->assertEquals('ES', State::translate('Espírito Santo'));
        $this->assertEquals('ES', State::translate('Espirito Santo'));
        $this->assertEquals('ES', State::translate('ESPÍRITO SANTO'));
        $this->assertEquals('ES', State::translate('ESPIRITO SANTO'));
    }
}