<?php

use \Winker\Integration\Util\Model\Translation;
use \Winker\Integration\Util\Model\Translation\Field\Zipcode;

class ZipcodeTest extends PHPUnit_Framework_TestCase {

    public function testTranslate() {
        $this->assertEquals('88123456', Zipcode::translate('88.123-456'));
        $this->assertEquals('88123456', Zipcode::translate('88123-456'));
        $this->assertEquals('88123456', Zipcode::translate('88123456'));
    }
}