<?php

declare(strict_types=1);

namespace Winker\Integration\Util\Tests\Model\Translation\Filter;


use Winker\Integration\Util\Model\Translation\Filter\DivisionFilterTrait;

/**
 * Class DivisionFilterTraitTest
 *
 * @package Winker\Integration\Util\Tests\Model\Translation\Filter
 */
class DivisionFilterTraitTest extends \TestCase
{

    use DivisionFilterTrait;

    /**
     * @var
     */
    private $externalId;

    /**
     * @return string
     */
    public function getFilterString()
    {
        return $this->externalId;
    }

    /**
     * @param string $filterString
     */
    public function setFilterString($filterString)
    {
        $this->externalId = $filterString;
    }

    /**
     * Check if trait has methods
     */
    public function testUseTraitHasMethod()
    {

        $this->assertTrue(method_exists($this, 'filterExternalId'));

    }

    /**
     *
     */
    public function testFilterExternalIdPortal()
    {

        $this->externalId = "Condominio#84|Divisao#208";
        $filtered = $this->filterExternalIdDivision();

        $this->assertEquals("Condominio#84|Divisao#208", $filtered);

        $this->externalId
            = "Condominio#84|Divisao#Principal|Unidade#123|Rateio#123";
        $filtered = $this->filterExternalIdDivision();

        $this->assertEquals("Condominio#84|Divisao#Principal", $filtered);

        $this->externalId = "Condominio#84|Divisao#208|Unidade#123";
        $filtered = $this->filterExternalIdDivision();

        $this->assertEquals("Condominio#84|Divisao#208", $filtered);

        $this->externalId = "Condominio#84|Divisao#208|Unidade#123|Rateio#123";
        $filtered = $this->filterExternalIdDivision();

        $this->assertEquals("Condominio#84|Divisao#208", $filtered);

        $this->externalId = "Condominio#84|Divisao#20/8|Unidade#123|Rateio#123";
        $filtered = $this->filterExternalIdDivision();

        $this->assertEquals("Condominio#84|Divisao#20/8", $filtered);

    }

}
