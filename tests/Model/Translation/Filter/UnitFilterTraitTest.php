<?php

declare(strict_types=1);

namespace Winker\Integration\Util\Tests\Model\Translation\Filter;


use Winker\Integration\Util\Model\Translation\Filter\UnitFilterTrait;

/**
 * Class UnitFilterTraitTest
 *
 * @package Winker\Integration\Util\Tests\Model\Translation\Filter
 */
class UnitFilterTraitTest extends \TestCase
{

    use UnitFilterTrait;

    /**
     * @var
     */
    private $externalId;

    /**
     * @return string
     */
    public function getFilterString()
    {
        return $this->externalId;
    }

    /**
     * @param string $filterString
     */
    public function setFilterString($filterString)
    {
        $this->externalId = $filterString;
    }


    /**
     * Check if trait has methods
     */
    public function testUseTraitHasMethod()
    {

        $this->assertTrue(method_exists($this, 'filterExternalId'));

    }

    /**
     *
     */
    public function testFilterExternalIdUnitSuperLogica()
    {
        $this->externalId = "45|Portal:6|Unit:238";
        $filtered = $this->filterExternalIdUnitSuperLogica();

        $this->assertEquals("45|Portal:6|Unit:238", $filtered);

        $this->externalId = "45|Portal:6|Unit:238|BillingUnit:250698";
        $filtered = $this->filterExternalIdUnitSuperLogica();

        $this->assertEquals("45|Portal:6|Unit:238", $filtered);

        $this->externalId = "45|Portal:6|Unit:sdfsdfdsf|BillingUnit:250698";
        $filtered = $this->filterExternalIdUnitSuperLogica();

        $this->assertEquals("45|Portal:6|Unit:sdfsdfdsf", $filtered);

        $this->externalId
            = "45|Portal:6|Unit:sdfsdfdsf234234|BillingUnit:250698";
        $filtered = $this->filterExternalIdUnitSuperLogica();

        $this->assertEquals("45|Portal:6|Unit:sdfsdfdsf234234", $filtered);


        $this->externalId = "45|Portal:6|Unit:238|UserUnit:14234";
        $filtered = $this->filterExternalIdUnitSuperLogica();

        $this->assertEquals("45|Portal:6|Unit:238", $filtered);

        $this->externalId
            = "45|Portal:6|Unit:238|UserUnit:14234|Uasdasd:asdsda|aasdas:asdas";
        $filtered = $this->filterExternalIdUnitSuperLogica();

        $this->assertEquals("45|Portal:6|Unit:238", $filtered);

    }

    /**
     * @expectedException    \Winker\Integration\Util\Exception\RetrieveUnitException
     * @expectedExceptionMessage External id inválido
     *
     */
    public function testFilterExternalIdPortalSuperLogicaInvalid()
    {

        $this->externalId = "Condominio#84|Divisao#208|Unidade#123";
        $this->filterExternalIdUnitSuperLogica();

    }


    /**
     * @expectedException    \Winker\Integration\Util\Exception\RetrieveUnitException
     * @expectedExceptionMessage External id inválido
     *
     */
    public function testFilterExternalIdPortalInvalid()
    {

        $this->externalId
            = "45|Portal:6|Unit:238|UserUnit:14234|Uasdasd:asdsda|aasdas:asdas";
        $this->filterExternalIdUnit();

    }

    /**
     * Test if external id is valid and return part of Unit
     */
    public function testFilterExternalIdUnit()
    {

        $this->externalId = "Condominio#84|Divisao#208|Unidade#123";
        $filtered = $this->filterExternalIdUnit();

        $this->assertEquals("Condominio#84|Divisao#208|Unidade#123", $filtered);

        $this->externalId = "Condominio#84|Divisao#208|Unidade#123|Rateio#123";
        $filtered = $this->filterExternalIdUnit();

        $this->assertEquals("Condominio#84|Divisao#208|Unidade#123", $filtered);

        $this->externalId = "Condominio#84|Divisao#208|Unidade#12/3|Rateio#123";
        $filtered = $this->filterExternalIdUnit();

        $this->assertEquals(
            "Condominio#84|Divisao#208|Unidade#12/3", $filtered
        );


    }

}
