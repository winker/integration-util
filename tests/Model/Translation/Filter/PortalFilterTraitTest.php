<?php

declare(strict_types=1);

namespace Winker\Integration\Util\Tests\Model\Translation\Filter;


use Winker\Integration\Util\Model\Translation\Filter\PortalFilterTrait;

/**
 * Class PortalFilterTraitTest
 *
 * @package Winker\Integration\Util\Tests\Model\Translation\Filter
 */
class PortalFilterTraitTest extends \TestCase
{

    use PortalFilterTrait;

    /**
     * @var
     */
    private $externalId;

    /**
     * @return string
     */
    public function getFilterString()
    {
        return $this->externalId;
    }

    /**
     * @param string $filterString
     */
    public function setFilterString($filterString)
    {
        $this->externalId = $filterString;
    }

    /**
     * Check if trait has methods
     */
    public function testUseTraitHasMethod()
    {

        $this->assertTrue(method_exists($this, 'filterExternalId'));

    }

    /**
     *
     */
    public function testFilterExternalIdPortalSuperLogica()
    {

        $this->externalId = "45|Portal:6";
        $filtered = $this->filterExternalIdPortalSuperLogica();

        $this->assertEquals("45|Portal:6", $filtered);

        $this->externalId = "45|Portal:6|Unit:238";
        $filtered = $this->filterExternalIdPortalSuperLogica();

        $this->assertEquals("45|Portal:6", $filtered);

        $this->externalId = "45|Portal:130|Unit:5237|BillingUnit:250698";
        $filtered = $this->filterExternalIdPortalSuperLogica();

        $this->assertEquals("45|Portal:130", $filtered);

        $this->externalId = "45|Portal:asdsad|Unit:5237|BillingUnit:250698";
        $filtered = $this->filterExternalIdPortalSuperLogica();

        $this->assertEquals("45|Portal:asdsad", $filtered);

        $this->externalId
            = "45|Portal:asdsad12222|Unit:5237|BillingUnit:250698";
        $filtered = $this->filterExternalIdPortalSuperLogica();

        $this->assertEquals("45|Portal:asdsad12222", $filtered);

        $this->externalId = "45|Portal:98|BankAccount:329";
        $filtered = $this->filterExternalIdPortalSuperLogica();

        $this->assertEquals("45|Portal:98", $filtered);

        $this->externalId = "45|Portal:132|Unit:5541|UserUnit:14234";
        $filtered = $this->filterExternalIdPortalSuperLogica();

        $this->assertEquals("45|Portal:132", $filtered);

    }

    /**
     * @expectedException    \Winker\Integration\Util\Exception\RetrievePortalException
     * @expectedExceptionMessage External id inválido
     *
     */
    public function testFilterExternalIdPortalSuperLogicaInvalid()
    {

        $this->externalId = "Condominio#84|PlanoContaContabilidade#208";
        $this->filterExternalIdPortalSuperLogica();
    }

    /**
     * @expectedException    \Winker\Integration\Util\Exception\RetrievePortalException
     * @expectedExceptionMessage External id inválido
     *
     */
    public function testFilterExternalIdPortalInvalid()
    {

        $this->externalId = "45|Portal:132|Unit:5541|UserUnit:14234";
        $this->filterExternalIdPortal();
    }

    /**
     *
     */
    public function testFilterExternalIdPortalFusion()
    {

        $this->externalId = "57|83719963000177.7576321903";
        $filtered         = $this->filterExternalIdPortalFusion();

        $this->assertEquals("57|83719963000177", $filtered);
    }

    /**
     *
     */
    public function testFilterExternalIdPortalFusionId()
    {

        $this->externalId = "57|83719963000177.7576321903";
        $filtered         = $this->filterExternalIdPortalFusionId();
        $this->assertEquals("83719963000177", $filtered);

        $this->externalId = "57|83719963000177.7576321903asdasdsad";
        $filtered         = $this->filterExternalIdPortalFusionId();
        $this->assertEquals("83719963000177", $filtered);
    }

    /**
     *
     */
    public function testFilterExternalIdPortal()
    {

        $this->externalId = "Condominio#84|PlanoContaContabilidade#208";
        $filtered = $this->filterExternalIdPortal();

        $this->assertEquals("Condominio#84", $filtered);


        $this->externalId = "Condominio#PRINCIAl|PlanoContaContabilidade#208";
        $filtered = $this->filterExternalIdPortal();

        $this->assertEquals("Condominio#PRINCIAl", $filtered);

        $this->externalId
            = "Condominio#PRINCIAl123123|PlanoContaContabilidade#208";
        $filtered = $this->filterExternalIdPortal();

        $this->assertEquals("Condominio#PRINCIAl123123", $filtered);

        $this->externalId = "Condominio#84|Divisao#208";
        $filtered = $this->filterExternalIdPortal();

        $this->assertEquals("Condominio#84", $filtered);

        $this->externalId = "Condominio#84|Divisao#208|Unidade#123";
        $filtered = $this->filterExternalIdPortal();

        $this->assertEquals("Condominio#84", $filtered);

        $this->externalId = "Condominio#84|Divisao#208|Unidade#123|Rateio#123";
        $filtered = $this->filterExternalIdPortal();

        $this->assertEquals("Condominio#84", $filtered);

        $this->externalId = "Condominio#84|Banco#208";
        $filtered = $this->filterExternalIdPortal();

        $this->assertEquals("Condominio#84", $filtered);

        $this->externalId = "Condominio#84asdasd|Banco#208";
        $filtered = $this->filterExternalIdPortal();

        $this->assertEquals("Condominio#84asdasd", $filtered);

        $this->externalId = "Condominio#84-asdsd|Banco#208";
        $filtered = $this->filterExternalIdPortal();

        $this->assertEquals("Condominio#84-asdsd", $filtered);

        $this->externalId = "Condominio#84-   asdsd|Banco#208";
        $filtered = $this->filterExternalIdPortal();

        $this->assertEquals("Condominio#84-   asdsd", $filtered);

        $this->externalId = "Condominio#84  32asf|Banco#208";
        $filtered = $this->filterExternalIdPortal();

        $this->assertEquals("Condominio#84  32asf", $filtered);

        $this->externalId
            = "Condominio#84  sdf - sdf32434|Divisao#208|Unidade#123|Rateio#123";
        $filtered = $this->filterExternalIdPortal();

        $this->assertEquals("Condominio#84  sdf - sdf32434", $filtered);

        $this->externalId
            = "Condominio#84  sdf - sdf324/34|Divisao#208|Unidade#123|Rateio#123";
        $filtered = $this->filterExternalIdPortal();

        $this->assertEquals("Condominio#84  sdf - sdf324/34", $filtered);


    }

    public function testFilterExternalIdPortalGetId()
    {

        $filtered = $this->filterExternalIdPortalGetId(
            "Condominio#84|PlanoContaContabilidade#208"
        );
        $this->assertEquals(84, $filtered);

        $filtered = $this->filterExternalIdPortalGetId(
            "Condominio#PRINCIAl|PlanoContaContabilidade#208"
        );

        $this->assertEquals('PRINCIAl', $filtered);

        $filtered = $this->filterExternalIdPortalGetId(
            "Condominio#PRINCIAl123123|PlanoContaContabilidade#208"
        );

        $this->assertEquals("PRINCIAl123123", $filtered);


        $filtered = $this->filterExternalIdPortalGetId(
            "Condominio#84|Divisao#208"
        );

        $this->assertEquals("84", $filtered);


        $filtered = $this->filterExternalIdPortalGetId(
            "Condominio#84|Divisao#208|Unidade#123"
        );

        $this->assertEquals(84, $filtered);

        $filtered = $this->filterExternalIdPortalGetId(
            "Condominio#84|Divisao#208|Unidade#123|Rateio#123"
        );

        $this->assertEquals(84, $filtered);

        $filtered = $this->filterExternalIdPortalGetId(
            "Condominio#84|Banco#208"
        );

        $this->assertEquals(84, $filtered);

        $filtered = $this->filterExternalIdPortalGetId(
            "Condominio#84asdasd|Banco#208"
        );

        $this->assertEquals("84asdasd", $filtered);

        $filtered = $this->filterExternalIdPortalGetId(
            "Condominio#84-asdsd|Banco#208"
        );

        $this->assertEquals("84-asdsd", $filtered);

        $filtered = $this->filterExternalIdPortalGetId(
            "Condominio#84-   asdsd|Banco#208"
        );

        $this->assertEquals("84-   asdsd", $filtered);

        $filtered = $this->filterExternalIdPortalGetId(
            "Condominio#84  32asf|Banco#208"
        );

        $this->assertEquals("84  32asf", $filtered);

        $filtered = $this->filterExternalIdPortalGetId(
            "Condominio#84  sdf - sdf32434|Divisao#208|Unidade#123|Rateio#123"
        );

        $this->assertEquals("84  sdf - sdf32434", $filtered);

        $filtered = $this->filterExternalIdPortalGetId(
            "45|Portal:132|Unit:5541|UserUnit:14234"
        );

        $this->assertEquals(132, $filtered);

        $filtered = $this->filterExternalIdPortalGetId(
            "45|Portal:132|Unit:5541"
        );

        $this->assertEquals(132, $filtered);


        $filtered = $this->filterExternalIdPortalGetId(
            "45|Portal:132"
        );

        $this->assertEquals(132, $filtered);

    }

}
