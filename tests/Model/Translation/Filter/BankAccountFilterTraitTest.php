<?php

declare(strict_types=1);

namespace Winker\Integration\Util\Tests\Model\Translation\Filter;


use Winker\Integration\Util\Model\Translation\Filter\BankAccountFilterTrait;


/**
 * Class BankAccountFilterTraitTest
 *
 * @package Winker\Integration\Util\Tests\Model\Translation\Filter
 */
class BankAccountFilterTraitTest extends \TestCase
{

    use BankAccountFilterTrait;

    /**
     * @var
     */
    private $externalId;

    /**
     * @return string
     */
    public function getFilterString()
    {
        return $this->externalId;
    }

    /**
     * @param string $filterString
     */
    public function setFilterString($filterString)
    {
        $this->externalId = $filterString;
    }

    /**
     * Check if trait has methods
     */
    public function testUseTraitHasMethod()
    {

        $this->assertTrue(method_exists($this, 'filterExternalId'));

    }

    /**
     *
     */
    public function testFilterExternalIdUnitSL()
    {

        $this->externalId = "45|Portal:143|BankAccount:476";
        $filtered = $this->filterExternalIdBankAccountSuperLogica();

        $this->assertEquals("45|Portal:143|BankAccount:476", $filtered);

        $this->externalId = "45|Portal:143|BankAccount:dfgdfgfdg";
        $filtered = $this->filterExternalIdBankAccountSuperLogica();

        $this->assertEquals("45|Portal:143|BankAccount:dfgdfgfdg", $filtered);

        $this->externalId = "45|Portal:143|BankAccount:4764535gdfgdfg";
        $filtered = $this->filterExternalIdBankAccountSuperLogica();

        $this->assertEquals(
            "45|Portal:143|BankAccount:4764535gdfgdfg", $filtered
        );

        $this->externalId
            = "45|Portal:143|BankAccount:476|Uasdasd:asdsda|aasdas:asdas";
        $filtered = $this->filterExternalIdBankAccountSuperLogica();

        $this->assertEquals("45|Portal:143|BankAccount:476", $filtered);

    }

    /**
     * @expectedException  \Winker\Integration\Util\Exception\RetrieveBankAccountException
     * @expectedExceptionMessage External id inválido
     *
     */
    public function testFilterExternalIdPortalSuperLogicaInvalid()
    {

        $this->externalId = "Condominio#1002|Banco#1574";
        $this->filterExternalIdBankAccountSuperLogica();
    }

    /**
     * @expectedException \Winker\Integration\Util\Exception\RetrieveBankAccountException
     * @expectedExceptionMessage External id inválido
     *
     */
    public function testFilterExternalIdPortalInvalid()
    {

        $this->externalId = "45|Portal:143|BankAccount:4764535gdfgdfg";
        $this->filterExternalIdBankAccount();

    }


    /**
     *
     */
    public function testFilterExternalIdUnit()
    {

        $this->externalId = "Condominio#1002|Banco#1574";
        $filtered = $this->filterExternalIdBankAccount();

        $this->assertEquals("Condominio#1002|Banco#1574", $filtered);

        $this->externalId = "Condominio#1002|Banco#1574|Unidade#123|Rateio#123";
        $filtered = $this->filterExternalIdBankAccount();

        $this->assertEquals("Condominio#1002|Banco#1574", $filtered);

        $this->externalId
            = "Condominio#1002|Banco#asdasd|Unidade#123|Rateio#123";
        $filtered = $this->filterExternalIdBankAccount();

        $this->assertEquals("Condominio#1002|Banco#asdasd", $filtered);

        $this->externalId
            = "Condominio#1002|Banco#1574asdsad|Unidade#123|Rateio#123";
        $filtered = $this->filterExternalIdBankAccount();

        $this->assertEquals("Condominio#1002|Banco#1574asdsad", $filtered);

        $this->externalId
            = "Condominio#1002|Banco#1574as/dsad|Unidade#123|Rateio#123";
        $filtered = $this->filterExternalIdBankAccount();

        $this->assertEquals("Condominio#1002|Banco#1574as/dsad", $filtered);


    }

}
