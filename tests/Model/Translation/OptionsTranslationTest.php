<?php

use \Winker\Integration\Util\Model\Translation\OptionsTranslation;

class OptionsTranslationTest extends PHPUnit_Framework_TestCase {
    public function testTranslate() {
        $options = [
            'proprietario'              => 'Owner',
            'proprietario_residente'    => 'Owner',
            'inquilino'                 => 'Tenant',
            'inquilino_residente'       => 'Tenant',
        ];

        $translation = new OptionsTranslation($options);

        $this->assertEquals('Owner', $translation->translate('proprietario'));
        $this->assertEquals('Owner', $translation->translate('proprietario_residente'));
        $this->assertEquals('Tenant', $translation->translate('inquilino'));
        $this->assertEquals('Tenant', $translation->translate('inquilino_residente'));
        $this->assertNull($translation->translate(''));
        $this->assertNull($translation->translate(null));
        $this->assertNull($translation->translate('option-not-found'));
    }

    public function testTranslateWithField() {
        $options = [
            'proprietario'              => 'Owner',
            'proprietario_residente'    => 'Owner',
            'inquilino'                 => 'Tenant',
            'inquilino_residente'       => 'Tenant',
        ];

        $translation = new OptionsTranslation($options, 'profile');
        $this->assertEquals('Owner', $translation->translateFromData(['name' => 'Name test', 'profile' => 'proprietario_residente']));
        $this->assertNull($translation->translateFromData(['name' => 'Name test', 'profile_2' => 'proprietario_residente']));
        $this->assertNull($translation->translateFromData([]));
        $this->assertNull($translation->translateFromData(null));

        $translation = new OptionsTranslation($options);
        $this->assertNull($translation->translateFromData(['name' => 'Name test', 'profile' => 'proprietario_residente']));
    }
}