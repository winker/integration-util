<?php

use \Winker\Integration\Util\Model\Translation\DateTranslation;

class DateTranslationTest extends PHPUnit_Framework_TestCase {
    public function testTranslate() {
        $this->assertEquals('2016-06-12', (new DateTranslation('d/m/Y'))->translate('12/06/2016'));
        $this->assertEquals('201606', (new DateTranslation(['d/m/Y' => 'Ym']))->translate('12/06/2016'));
        $this->assertEquals('2016-06-12 12:20:00', (new DateTranslation(['d/m/Y' => 'Y-m-d H:i:s']))->translate('12/06/2016 12:20:00'));
        $this->assertEquals('2016-06-12T12:20:00+0000', (new DateTranslation(['d/m/Y' => DateTime::ISO8601]))->translate('12/06/2016 12:20:00'));
        $this->assertEquals('2016-06-25', (new DateTranslation('m/d/Y'))->translate('06/25/2016'));
        $this->assertEquals('2016-06-12', (new DateTranslation('d/m/Y'))->translate('12/06/2016 23:59:59'));
        $this->assertEquals('2016-06-12', (new DateTranslation('d/m/Y'))->translate('12/06/2016 00:00:00'));
        $this->assertEquals('2016-06-12', (new DateTranslation('d/m/Y'))->translate('12/06/2016 12:20:00'));
        $this->assertEquals('2016-06-12', (new DateTranslation('d/m/Y'))->translate('12/06/2016 12:20:00'));
        $this->assertNull((new DateTranslation('d/m/Y'))->translate(''));
        $this->assertNull((new DateTranslation('d/m/Y'))->translate(null));
    }

    public function testTranslate_invalid_date() {
        $error = null;

        try {
            $this->assertEquals('2016-06-12', (new DateTranslation('d/m/Y'))->translate('xxxx'));
        } catch (\Winker\Integration\Util\Model\Translation\Exception $e) {
            $error = $e->getMessage();
        }

        $this->assertEquals('Invalid date "xxxx" with "d/m/Y" format.', $error);
    }

    public function testTranslateWithField() {
        $dateTranslation = new DateTranslation('d/m/Y', 'created_at');

        $this->assertEquals('2016-06-12', $dateTranslation->translateFromData(['name' => 'Test', 'created_at' => '12/06/2016']));
    }

    public function testTranslateWithFieldNotFound() {
        $dateTranslation = new DateTranslation('d/m/Y', 'created_at');

        $this->assertNull($dateTranslation->translateFromData(['name' => 'Test']));
    }
}