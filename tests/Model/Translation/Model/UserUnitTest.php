<?php

use \Winker\Integration\Util\Model\Translation;
use \Winker\Integration\Util\Model\Translation\Model\UserUnit;
use \Winker\Integration\Util\Model\Translation\OptionsTranslation;

class UserUnitTest extends PHPUnit_Framework_TestCase {

    private $exampleData;
    private $exampleTranslate;

    protected function setUp() {
        $this->exampleData = [
            'id_condominio'         => '1',
            'id_unidade_uni'        => '23',
            'id_contato_con'        => '46',
            'st_nome_con'           => 'Felipe Amaral',
            'st_telefone_con'       => '(48) 2020-0000',
            'st_fax_con'            => '(48) 9191-0000',
            'st_email_con'          => 'proprietario@email.com',
            'st_cpf_con'            => '363.201.434-52',
            'st_rg_con'             => '123456-7',
            'st_nometiporesp_tres'  => 'Proprietário Residente',
            'st_endereco_con'       => 'Rua do Portal, 123',
            'st_bairro_con'         => 'Nome do Bairro',
            'st_cep_con'            => '88000-123',
            'st_cidade_con'         => 'Florianópolis',
            'id_uf_uf'              => '42',
        ];

        $this->exampleTranslate = [
            'id_unit'               => 'id_unidade_uni',
            'id_portal'             => 'id_condominio',
            'id_user_unit'          => 'id_contato_con',
            'name'                  => 'st_nome_con',
            'phone'                 => 'st_telefone_con',
            'cellphone'             => 'st_fax_con',
            'email'                 => 'st_email_con',
            'doc_cpf'               => 'st_cpf_con',
            'doc_rg'                => 'st_rg_con',
            'profile'               => new OptionsTranslation(['Proprietário Residente' => 'Owner'], 'st_nometiporesp_tres'),
            'address'                => 'st_endereco_con',
            'address_neighborhood'   => 'st_bairro_con',
            'address_zipcode'        => 'st_cep_con',
            'address_city'           => 'st_cidade_con',
            'address_state'          => 'id_uf_uf',
        ];
    }

    public function testTranslate() {
        $translated = Translation::translate($this->exampleData, $this->exampleTranslate, UserUnit::class);

        $this->assertArraySubset([
            'unique_id'             => 'Portal:1|Unit:23|UserUnit:46',
            'id_portal'             => '1',
            'id_user_unit'          => '46',
            'name'                  => 'Felipe Amaral',
            'phone'                 => '(48) 2020-0000',
            'cellphone'             => '(48) 9191-0000',
            'email'                 => 'proprietario@email.com',
            'doc_cpf'               => '36320143452',
            'doc_rg'                => '123456-7',
            'profile'               => 'Owner',
            'address'                => 'Rua do Portal, 123',
            'address_neighborhood'   => 'Nome do Bairro',
            'address_zipcode'        => '88000123',
            'address_city'           => 'Florianópolis',
            'address_state'          => 'SC',
            'relations'             => [
                'Portal'    => 'Portal:1',
                'Unit'      => 'Portal:1|Unit:23',
            ]
        ], $translated);
    }
}