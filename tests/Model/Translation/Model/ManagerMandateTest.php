<?php

use \Winker\Integration\Util\Model\Translation;
use \Winker\Integration\Util\Model\Translation\Model\ManagerMandate;

class ManagerMandateTest extends PHPUnit_Framework_TestCase {

    private $exampleData;
    private $exampleTranslate;

    protected function setUp() {
        $this->exampleData = json_decode(file_get_contents(__DIR__ . '/data/manager_mandates.json'), true);

        $this->exampleTranslate = [
            'id_manager_mandate'    => 'id_sindico_sin',
            'id_portal'             => 'id_condominio_cond',
            'start'                 => new Translation\DateTranslation('m/d/Y', 'dt_entrada_sin'),
            'end'                   => new Translation\DateTranslation('m/d/Y', 'dt_saida_sin'),
            'email'                 => 'st_email_sin',
            'first_name'            => 'st_nome_sin',
            'last_name'             => null,
            'cellphone'             => 'st_celular_sin',
            'phone'                 => 'st_telefone_sin',
            'status'                => null
        ];
    }

    public function testTranslate() {
        $translatedList = Translation::translateList($this->exampleData, $this->exampleTranslate, ManagerMandate::class);
        $portals        = [];

        foreach ($translatedList as $translated) {
            if (!isset($portals[$translated['id_portal']])) {
                $portals[$translated['id_portal']] = [];
            }

            $portals[$translated['id_portal']][$translated['id_manager_mandate']] = $translated;
        }

        foreach ($portals as &$mandatosOrder) {
            krsort($mandatosOrder);
        }

        $translatedListWithStatus = [];

        foreach ($portals as $mandatos) {
            $i = 0;
            foreach ($mandatos as $mandato) {
                if ($i++ === 0) {
                    $mandato['status'] = 'present';
                } else {
                    $mandato['status'] = 'previous';
                }

                $translatedListWithStatus[] = $mandato;
            }
        }

        $this->assertArraySubset(
            json_decode(file_get_contents(__DIR__ . '/data/manager_mandates_translated.json'), true),
            $translatedListWithStatus
        );
    }
}