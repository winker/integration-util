<?php

use \Winker\Integration\Util\Model\Translation;
use \Winker\Integration\Util\Model\Translation\Model\Portal;

class PortalTest extends PHPUnit_Framework_TestCase {

    private $exampleData;
    private $exampleTranslate;

    protected function setUp() {
        $this->exampleData = [
            'id_condominio' => '123',
            'fantasia_cond' => 'Ilhas Belas',
            'nome_cond'     => 'Condomínio Residencial Ilhas Belas',
            'cnpj_cond'     => '93.329.359/0001-98',
            'telefone'      => '(48) 3123-4567',
            'endereco_cond' => 'Rua do Condomínio',
            'complemento'   => 'Ao lado do Alcantara',
            'bairro'        => 'Caminho da Moca',
            'cidade'        => 'SÃO JOSÉ',
            'estado'        => '42',
            'cep'           => '88.123-456',
        ];

        $this->exampleTranslate = [
            'unique_id'             => ['Portal' => 'id_condominio'],
            'name'                  => ['fantasia_cond', 'nome_cond'],
            'doc_cnpj'              => 'cnpj_cond',
            'phone'                 => 'telefone',
            'address'                => 'endereco_cond',
            'address_complement'     => 'complemento',
            'address_neighborhood'   => 'bairro',
            'address_city'           => 'cidade',
            'address_state'          => 'estado',
            'address_zipcode'        => 'cep',
        ];
    }

    public function testTranslate() {
        $translated = Translation::translate($this->exampleData, $this->exampleTranslate, Portal::class);

        $this->assertArraySubset([
            'unique_id'             => 'Portal:123',
            'name'                  => 'Ilhas Belas',
            'doc_cnpj'              => '93329359000198',
            'phone'                 => '(48) 3123-4567',
            'address'                => 'Rua do Condomínio',
            'address_complement'     => 'Ao lado do Alcantara',
            'address_neighborhood'   => 'Caminho da Moca',
            'address_city'           => 'SÃO JOSÉ',
            'address_state'          => 'SC',
            'address_zipcode'        => '88123456',
        ], $translated);
    }

    public function testTranslateList() {
        $translated = Translation::translateList([$this->exampleData, $this->exampleData], $this->exampleTranslate, Portal::class);

        $this->assertArraySubset([
            [
                'unique_id'             => 'Portal:123',
                'name'                  => 'Ilhas Belas',
                'doc_cnpj'              => '93329359000198',
                'phone'                 => '(48) 3123-4567',
                'address'                => 'Rua do Condomínio',
                'address_complement'     => 'Ao lado do Alcantara',
                'address_neighborhood'   => 'Caminho da Moca',
                'address_city'           => 'SÃO JOSÉ',
                'address_state'          => 'SC',
                'address_zipcode'        => '88123456',
            ],
            [
                'unique_id'             => 'Portal:123',
                'name'                  => 'Ilhas Belas',
                'doc_cnpj'              => '93329359000198',
                'phone'                 => '(48) 3123-4567',
                'address'                => 'Rua do Condomínio',
                'address_complement'     => 'Ao lado do Alcantara',
                'address_neighborhood'   => 'Caminho da Moca',
                'address_city'           => 'SÃO JOSÉ',
                'address_state'          => 'SC',
                'address_zipcode'        => '88123456',
            ]
        ], $translated);
    }
}