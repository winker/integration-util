<?php

use \Winker\Integration\Util\Model\Translation;
use \Winker\Integration\Util\Model\Translation\Model\BillingUnit;
use \Winker\Integration\Util\Model\Translation\DateTranslation;

class BillingUnitTest extends PHPUnit_Framework_TestCase {

    private $exampleData;
    private $exampleTranslate;

    protected function setUp() {
        $this->exampleData = [
            'id_condominio'         => '1',
            'id_unidade_uni'        => '23',
            'id_contabanco_cb'      => '4',
            'id_recebimento_recb'   => '120',

            'vl_emitido_recb'       =>  '567.89',
            'dt_vencimento_recb'    => '05/19/2016 00:00:00',
            'dt_geracao_recb'       => '04/13/2016 00:00:00',
            'dt_competencia_recb'   => '04/01/2016 00:00:00',
            'dt_cancelamento_recb'  => '04/25/2016',

            'link_segundavia' => 'http://site/uri?id=1&key=12345',

            'dt_liquidacao_recb'    => '',
            'vl_total_recb'         => '567.89',
        ];

        $this->exampleTranslate = [
            'id_portal'             => 'id_condominio',
            'id_unit'               => 'id_unidade_uni',
            'id_bank_account'       => 'id_contabanco_cb',
            'id_billing_unit'       => 'id_recebimento_recb',

            'amount'            => 'vl_emitido_recb',
            'due_date'          => new DateTranslation('m/d/Y', 'dt_vencimento_recb'),
            'created_date'      => new DateTranslation('m/d/Y', 'dt_geracao_recb'),
            'reference_date'    => new DateTranslation(['m/d/Y' => 'Ym'], 'dt_competencia_recb'),
            'cancel_date'       => new DateTranslation('m/d/Y', 'dt_cancelamento_recb'),
            'document_number'   => 'id_recebimento_recb',

            'url_billing_file'  => 'link_segundavia',

            'payment_date'          => new DateTranslation('m/d/Y', 'dt_liquidacao_recb'),
            'amount_paid'           => function($data) {
                if (!empty($data['vl_total_recb']) & !empty($data['dt_liquidacao_recb'])) {
                    return $data['vl_total_recb'];
                }
            },
        ];
    }

    public function testTranslate_new() {
        $translated = Translation::translate($this->exampleData, $this->exampleTranslate, BillingUnit::class);

        $this->assertArraySubset([
            'unique_id'         => 'Portal:1|Unit:23|BillingUnit:120',
            'id_portal'         => '1',
            'id_unit'           => '23',
            'id_bank_account'   => '4',
            'id_billing_unit'   => '120',
            'amount'            => '567.89',
            'document_number'   => '120',
            'url_billing_file'  => 'http://site/uri?id=1&key=12345',
            'due_date'          => '2016-05-19',
            'created_date'      => '2016-04-13',
            'reference_date'    => '201604',
            'cancel_date'       => '2016-04-25',
            'payment_date'      => '',
            'amount_paid'       => '',
            'relations'             => [
                'Portal'        => 'Portal:1',
                'Unit'          => 'Portal:1|Unit:23',
                'BankAccount'   => 'Portal:1|BankAccount:4',
            ]
        ], $translated);
    }

    public function testTranslate_paid() {
        $data = $this->exampleData;
        $data['dt_liquidacao_recb'] = '06/29/2016';
        $data['vl_total_recb']      = '720.35';

        $translated = Translation::translate($data, $this->exampleTranslate, BillingUnit::class);

        $this->assertArraySubset([
            'unique_id'         => 'Portal:1|Unit:23|BillingUnit:120',
            'id_portal'         => '1',
            'id_unit'           => '23',
            'id_bank_account'   => '4',
            'id_billing_unit'   => '120',
            'amount'            => '567.89',
            'document_number'   => '120',
            'url_billing_file'  => 'http://site/uri?id=1&key=12345',
            'due_date'          => '2016-05-19',
            'created_date'      => '2016-04-13',
            'reference_date'    => '201604',
            'cancel_date'       => '2016-04-25',
            'payment_date'      => '2016-06-29',
            'amount_paid'       => '720.35',
            'relations'             => [
                'Portal'        => 'Portal:1',
                'Unit'          => 'Portal:1|Unit:23',
                'BankAccount'   => 'Portal:1|BankAccount:4',
            ]
        ], $translated);
    }
}