<?php

use \Winker\Integration\Util\Model\Translation;
use \Winker\Integration\Util\Model\Translation\Model\Unit;

class UnitTest extends PHPUnit_Framework_TestCase {

    private $exampleData;
    private $exampleTranslate;

    protected function setUp() {
        $this->exampleData = [
            'id_condominio'     => '1',
            'id_unidade_uni'    => '23',
            'st_unidade_uni'    => '101',
            'st_bloco_uni'      => 'Bloco A',
            'nm_fracao_uni'     => '0.25000',
        ];

        $this->exampleTranslate = [
            'id_portal'             => 'id_condominio',
            'id_unit'               => 'id_unidade_uni',
            'division'              => 'st_bloco_uni',
            'name'                  => 'st_unidade_uni',
            'fractional_ownership'  => 'nm_fracao_uni',
        ];
    }

    public function testTranslate() {
        $translated = Translation::translate($this->exampleData, $this->exampleTranslate, Unit::class);

        $this->assertArraySubset([
            'unique_id'             => 'Portal:1|Unit:23',
            'id_portal'             => '1',
            'id_unit'               => '23',
            'division'              => 'Bloco A',
            'name'                  => '101',
            'fractional_ownership'  => '0.25000',
            'relations'             => [
                'Portal' => 'Portal:1'
            ]
        ], $translated);
    }
}