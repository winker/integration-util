<?php

use \Winker\Integration\Util\Model\Translation;
use \Winker\Integration\Util\Model\Translation\Model\Portal;
use \Winker\Integration\Util\Model\Translation\Model\CustomModel;

class CustomModelTest extends PHPUnit_Framework_TestCase {

    public function testTranslateCustomModel() {
        $customModel = new CustomModel();
        $customModel->model     = Portal::class;
        $customModel->fields    = [
            'address_state' => function($value) {
                $states = [
                    10 => 'MA',
                    11 => 'MG',
                    12 => 'MS',
                    13 => 'MT',
                    14 => 'PA',
                    15 => 'PB',
                ];

                if (isset($states[$value])) {
                    return $states[$value];
                } else {
                    return $value;
                }
            }
        ];

        $this->assertArraySubset(
            ['name' => 'Ilhas', 'address_state' => 'MA'],
            Translation::translate(['nome' => 'Ilhas', 'estado' => 10], ['name' => 'nome', 'address_state' => 'estado'], $customModel)
        );

        $this->assertArraySubset(
            ['name' => 'Ilhas', 'address_state' => 'MG'],
            Translation::translate(['nome' => 'Ilhas', 'estado' => 11], ['name' => 'nome', 'address_state' => 'estado'], $customModel)
        );

        $this->assertArraySubset(
            ['name' => 'Ilhas', 'address_state' => 'MS'],
            Translation::translate(['nome' => 'Ilhas', 'estado' => 12], ['name' => 'nome', 'address_state' => 'estado'], $customModel)
        );

        $this->assertArraySubset(
            ['name' => 'Ilhas', 'address_state' => 'MT'],
            Translation::translate(['nome' => 'Ilhas', 'estado' => 13], ['name' => 'nome', 'address_state' => 'estado'], $customModel)
        );

        $this->assertArraySubset(
            ['name' => 'Ilhas', 'address_state' => 'PA'],
            Translation::translate(['nome' => 'Ilhas', 'estado' => 14], ['name' => 'nome', 'address_state' => 'estado'], $customModel)
        );

        $this->assertArraySubset(
            ['name' => 'Ilhas', 'address_state' => 'PB'],
            Translation::translate(['nome' => 'Ilhas', 'estado' => 15], ['name' => 'nome', 'address_state' => 'estado'], $customModel)
        );

        $this->assertArraySubset(
            ['name' => 'Ilhas', 'address_state' => '16'],
            Translation::translate(['nome' => 'Ilhas', 'estado' => 16], ['name' => 'nome', 'address_state' => 'estado'], $customModel)
        );
    }
}