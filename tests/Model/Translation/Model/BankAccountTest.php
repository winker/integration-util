<?php

use \Winker\Integration\Util\Model\Translation;
use \Winker\Integration\Util\Model\Translation\Model\BankAccount;

class BankAccountTest extends PHPUnit_Framework_TestCase {

    private $exampleData;
    private $exampleTranslate;

    protected function setUp() {
        $this->exampleData = [
            'id_condominio'     => '123',
            'id_banco_conta'    => '890',
            'nome'              => 'Banco do Brasil',
            'agencia'           => '1234-5',
            'conta'             => '5678-9',
        ];

        $this->exampleTranslate = [
            'id_portal'             => 'id_condominio',
            'id_bank_account'       => 'id_banco_conta',
            'name'                  => 'nome+conta',
            'agency_number'         => 'agencia',
            'agency_number_digit'   => 'agencia',
            'account_number'        => 'conta',
            'account_number_digit'  => 'conta',
        ];
    }

    public function testTranslate() {
        $translated = Translation::translate($this->exampleData, $this->exampleTranslate, BankAccount::class);

        $this->assertArraySubset([
            'unique_id'             => 'Portal:123|BankAccount:890',
            'id_portal'             => '123',
            'id_bank_account'       => '890',
            'name'                  => 'Banco do Brasil 5678-9',
            'agency_number'         => '1234',
            'agency_number_digit'   => '5',
            'account_number'        => '5678',
            'account_number_digit'  => '9',
            'relations'             => [
                'Portal' => 'Portal:123'
            ]
        ], $translated);
    }
}