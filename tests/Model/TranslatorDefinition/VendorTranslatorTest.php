<?php

use \Winker\Integration\Util\Model\TranslatorDefinition\VendorTranslator;
use \Winker\Integration\Util\Http\ResponseApi;

class VendorTranslatorTest extends TestCase {

    /**
     * @var PortalTranslator
     */
    private $portalTranslator;

    /**
     * @var \Winker\Integration\Util\Integration\Api
     */
    private $winkerClient;

    /**
     * @var \Winker\Integration\Util\Integration\Api
     */
    private $vendorClient;

    public function setUp() {
        $this->portalTranslator = new PortalTranslator();

        $this->vendorClient = new \Winker\Integration\Util\Http\Client();
        $this->vendorClient->setDriver(new MyDriver());

        $this->winkerClient = new \Winker\Integration\Util\Integration\Api();
    }

    public function testCollection() {
        $this->vendorClient->mockJsonResponse('{"status":"200","session":"test_session","msg":"","data":[{"data_fechamento":"","st_cep_cond":"88000-000","st_url_cond":"","st_inscrestadual_cond":"","fl_tipocond_cond":"","id_condominio_cond":"1","id_planoconta_plc":"8","dt_diadebito_cond":"0","dt_diavencimento_cond":"10","id_tipocobranca_tco":"1","st_cnpj_cond":"","st_email_cond":"","st_cpf_cond":"","st_estado_cond":"23","st_telefone_cond":"","st_observacao_cond":"","st_fax_cond":"","st_nome_cond":"Condominio Teste","st_endereco_cond":"Rua de Teste","st_complemento_cond":"","st_bairro_cond":"Cidade Bela","st_cidade_cond":"S\u00e3o Jos\u00e9","st_fantasia_cond":"Condominio Teste","fl_ativo_cond":"1","nm_txfundocx_cond":"0.00","nm_txjuros_cond":"1.00","nm_txmulta_cond":"2.00","st_inadimplente_cond":"0.00","id_tipocondominio_tcon":"1","id_tipojuros_cond":"1","fl_jurosatualizar_vl_base_cond":"1","fl_multaatualizar_vl_base_cond":"1","nm_diasparaatualizar_cond":"","fl_juroscompostos_cond":"1","fl_multaprorata_cond":"","nm_multamaxima_cond":"","st_label_cond":"","nm_txhonorario_cond":"","id_contato_vazia_con":"","nm_txdesconto_cond":"","nm_descontoatedia_cond":"0","fl_descontovalorfixo_cond":"0","fl_multasobrejuros_cond":"0","nm_txdesconto2_cond":"","nm_txdesconto3_cond":"","nm_descontoatedia2_cond":"","nm_descontoatedia3_cond":"","fl_descontovalorfixo2_cond":"","fl_descontovalorfixo3_cond":"","fl_descontoapenascontas_cond":"","dt_fechamento_cond":"01\/01\/1899","id_licitamais_cond":"","st_token_emp":"","st_numeroendereco_cond":""},{"data_fechamento":"","st_cep_cond":"88123-345","st_url_cond":"","st_inscrestadual_cond":"","fl_tipocond_cond":"","id_condominio_cond":"2","id_planoconta_plc":"8","dt_diadebito_cond":"0","dt_diavencimento_cond":"20","id_tipocobranca_tco":"1","st_cnpj_cond":"","st_email_cond":"","st_cpf_cond":"51213880000115","st_estado_cond":"23","st_telefone_cond":"","st_observacao_cond":"","st_fax_cond":"","st_nome_cond":"Ilhas Belas","st_endereco_cond":"Servid\u00e3o de Teste","st_complemento_cond":"","st_bairro_cond":"Ipiranga","st_cidade_cond":"S\u00e3o Jos\u00e9","st_fantasia_cond":"Ilhas Belas","fl_ativo_cond":"1","nm_txfundocx_cond":"0.00","nm_txjuros_cond":"1.00","nm_txmulta_cond":"2.00","st_inadimplente_cond":"0.00","id_tipocondominio_tcon":"1","id_tipojuros_cond":"1","fl_jurosatualizar_vl_base_cond":"1","fl_multaatualizar_vl_base_cond":"1","nm_diasparaatualizar_cond":"180","fl_juroscompostos_cond":"1","fl_multaprorata_cond":"","nm_multamaxima_cond":"","st_label_cond":"","nm_txhonorario_cond":"","id_contato_vazia_con":"","nm_txdesconto_cond":"","nm_descontoatedia_cond":"0","fl_descontovalorfixo_cond":"0","fl_multasobrejuros_cond":"0","nm_txdesconto2_cond":"","nm_txdesconto3_cond":"","nm_descontoatedia2_cond":"","nm_descontoatedia3_cond":"","fl_descontovalorfixo2_cond":"","fl_descontovalorfixo3_cond":"","fl_descontoapenascontas_cond":"","dt_fechamento_cond":"01\/01\/1899","id_licitamais_cond":"","st_token_emp":"","st_numeroendereco_cond":""}],"executiontime":"0.2644s","url":""}');

        $collection = $this->collection();

        $this->assertArraySubset([
            [
                'unique_id'             => 'Portal:1',
                'name'                  => 'Condominio Teste',
                'doc_cnpj'              => '',
                'phone'                 => '',
                'address'                => 'Rua de Teste',
                'address_complement'     => '',
                'address_neighborhood'   => 'Cidade Bela',
                'address_city'           => 'São José',
                'address_state'          => 'CE',
                'address_zipcode'        => '88000000',
            ],
            [
                'unique_id'             => 'Portal:2',
                'name'                  => 'Ilhas Belas',
                'doc_cnpj'              => '51213880000115',
                'phone'                 => '',
                'address'                => 'Servidão de Teste',
                'address_complement'     => '',
                'address_neighborhood'   => 'Ipiranga',
                'address_city'           => 'São José',
                'address_state'          => 'CE',
                'address_zipcode'        => '88123345',
            ]
        ], $collection);
    }

    public function testRead() {
        $this->vendorClient->mockJsonResponse('{"status":"200","session":"test_session","msg":"","data":[{"condominio":[{"st_fantasia_cond":"Condominio Teste","id_planoconta_plc":"8","st_inadimplente_cond":"0.00","id_tipocobranca_tco":"1","dt_diavencimento_cond":"10","dt_diadebito_cond":"0","nm_txfundocx_cond":"0.00","id_tipocondominio_tcon":"1","st_cpf_cond":"","st_nome_cond":"Condominio Teste","id_tipojuros_cond":"1","st_label_cond":"","st_endereco_cond":"Rua de Teste","st_complemento_cond":"","st_bairro_cond":"Cidade Bela","st_cidade_cond":"S\u00e3o Jos\u00e9","st_cep_cond":"88000-000","nm_txjuros_cond":"1.00","nm_txmulta_cond":"2.00","nm_txhonorario_cond":"","nm_diasparaatualizar_cond":"","id_contato_vazia_con":"","st_telefone_cond":"","nm_txdesconto_cond":"","nm_txdesconto2_cond":"","nm_txdesconto3_cond":"","nm_descontoatedia_cond":"0","nm_descontoatedia2_cond":"","nm_descontoatedia3_cond":"","st_inscrestadual_cond":"","st_email_cond":"","st_url_cond":"","st_fax_cond":"","st_observacao_cond":"","fl_descontovalorfixo_cond":"0","fl_descontovalorfixo2_cond":"","fl_descontovalorfixo3_cond":"","fl_descontoapenascontas_cond":"","id_licitamais_cond":"","id_condominio_cond":"1","st_estado_cond":"23","fl_ativo_cond":"1","st_uf_uf":"SC","nm_iniciomes":"","st_incrementar":""}]}],"executiontime":"0.5811s"}');

        $read = $this->read('Portal:1');

        $this->assertArraySubset([
            'unique_id'             => 'Portal:1',
            'name'                  => 'Condominio Teste',
            'doc_cnpj'              => '',
            'phone'                 => '',
            'address'                => 'Rua de Teste',
            'address_complement'     => '',
            'address_neighborhood'   => 'Cidade Bela',
            'address_city'           => 'São José',
            'address_state'          => 'CE',
            'address_zipcode'        => '88000000',
        ], $read);
    }

    public function testCatchAndSync_from_uniquesIds() {
        $this->vendorClient->mockJsonResponses([
            '{"status":"200","session":"test_session","msg":"","data":[{"condominio":[{"st_fantasia_cond":"Condominio Teste","id_planoconta_plc":"8","st_inadimplente_cond":"0.00","id_tipocobranca_tco":"1","dt_diavencimento_cond":"10","dt_diadebito_cond":"0","nm_txfundocx_cond":"0.00","id_tipocondominio_tcon":"1","st_cpf_cond":"","st_nome_cond":"Condominio Teste","id_tipojuros_cond":"1","st_label_cond":"","st_endereco_cond":"Rua de Teste","st_complemento_cond":"","st_bairro_cond":"Cidade Bela","st_cidade_cond":"S\u00e3o Jos\u00e9","st_cep_cond":"88000-000","nm_txjuros_cond":"1.00","nm_txmulta_cond":"2.00","nm_txhonorario_cond":"","nm_diasparaatualizar_cond":"","id_contato_vazia_con":"","st_telefone_cond":"","nm_txdesconto_cond":"","nm_txdesconto2_cond":"","nm_txdesconto3_cond":"","nm_descontoatedia_cond":"0","nm_descontoatedia2_cond":"","nm_descontoatedia3_cond":"","st_inscrestadual_cond":"","st_email_cond":"","st_url_cond":"","st_fax_cond":"","st_observacao_cond":"","fl_descontovalorfixo_cond":"0","fl_descontovalorfixo2_cond":"","fl_descontovalorfixo3_cond":"","fl_descontoapenascontas_cond":"","id_licitamais_cond":"","id_condominio_cond":"1","st_estado_cond":"23","fl_ativo_cond":"1","st_uf_uf":"SC","nm_iniciomes":"","st_incrementar":""}]}],"executiontime":"0.5811s"}',
            '{"status":"200","session":"test_session","msg":"","data":[{"condominio":[{"st_fantasia_cond":"Condominio Teste","id_planoconta_plc":"8","st_inadimplente_cond":"0.00","id_tipocobranca_tco":"1","dt_diavencimento_cond":"10","dt_diadebito_cond":"0","nm_txfundocx_cond":"0.00","id_tipocondominio_tcon":"1","st_cpf_cond":"","st_nome_cond":"Condominio Teste","id_tipojuros_cond":"1","st_label_cond":"","st_endereco_cond":"Rua de Teste","st_complemento_cond":"","st_bairro_cond":"Cidade Bela","st_cidade_cond":"S\u00e3o Jos\u00e9","st_cep_cond":"88000-000","nm_txjuros_cond":"1.00","nm_txmulta_cond":"2.00","nm_txhonorario_cond":"","nm_diasparaatualizar_cond":"","id_contato_vazia_con":"","st_telefone_cond":"","nm_txdesconto_cond":"","nm_txdesconto2_cond":"","nm_txdesconto3_cond":"","nm_descontoatedia_cond":"0","nm_descontoatedia2_cond":"","nm_descontoatedia3_cond":"","st_inscrestadual_cond":"","st_email_cond":"","st_url_cond":"","st_fax_cond":"","st_observacao_cond":"","fl_descontovalorfixo_cond":"0","fl_descontovalorfixo2_cond":"","fl_descontovalorfixo3_cond":"","fl_descontoapenascontas_cond":"","id_licitamais_cond":"","id_condominio_cond":"2","st_estado_cond":"23","fl_ativo_cond":"1","st_uf_uf":"SC","nm_iniciomes":"","st_incrementar":""}]}],"executiontime":"0.5811s"}',
        ]);

        $this->winkerClient->mockJsonResponse('{"success":1, "data":[{"queue_added":1,"unique_id":"Portal:1","protocol":"6492c2ff-8d4e-4d4b-ad60-abc59971d1c4"},{"queue_added":1,"unique_id":"Portal:2","protocol":"b217273a-a8b5-4a3c-8fc0-6fbfb2fe9060"}]}');

        $responses = $this->catchAndSync(['Portal:1', 'Portal:2']);

        $this->assertArraySubset([
            'success'   => 1,
            'data'      => [
                [
                    'queue_added'   => 1,
                    'unique_id'     => 'Portal:1',
                    'protocol'      => '6492c2ff-8d4e-4d4b-ad60-abc59971d1c4',
                ],
                [
                    'queue_added'   => 1,
                    'unique_id'     => 'Portal:2',
                    'protocol'      => 'b217273a-a8b5-4a3c-8fc0-6fbfb2fe9060',
                ]
            ]
        ], $responses);
    }

    public function testCatchAndSyncAll() {
        $response1 = ['status' => 200, 'data' => []];
        $response2 = ['status' => 200, 'data' => []];
        $response3 = ['status' => 200, 'data' => []];
        $response4 = ['status' => 200, 'data' => []];

        $responseQueue1 = ['success' => true, 'data' => []];
        $responseQueue2 = ['success' => true, 'data' => []];
        $responseQueue3 = ['success' => true, 'data' => []];

        for ($i = 1; $i <=5 ;$i++) {
            $response1['data'][] = ['id_condominio_cond' => $i, 'st_nome_cond' => "Portal name {$i}"];
            $responseQueue1['data'][] = ['queue_added' => 1, 'unique_id' => "Portal:{$i}", 'protocol' => "prot-{$i}"];
        }

        for ($i = 6; $i <=10 ;$i++) {
            $response2['data'][] = ['id_condominio_cond' => $i, 'st_nome_cond' => "Portal name {$i}"];
            $responseQueue2['data'][] = ['queue_added' => 1, 'unique_id' => "Portal:{$i}", 'protocol' => "prot-{$i}"];
        }

        for ($i = 11; $i <=13 ;$i++) {
            $response3['data'][] = ['id_condominio_cond' => $i, 'st_nome_cond' => "Portal name {$i}"];
            $responseQueue3['data'][] = ['queue_added' => 1, 'unique_id' => "Portal:{$i}", 'protocol' => "prot-{$i}"];
        }

        $this->vendorClient->mockJsonResponses([
            json_encode($response1),
            json_encode($response2),
            json_encode($response3),
            json_encode($response4),
        ]);

        $this->winkerClient->mockJsonResponses([
            json_encode($responseQueue1),
            json_encode($responseQueue2),
            json_encode($responseQueue3),
        ]);

        $responses = $this->catchAndSyncAll();

        $this->assertArraySubset([
            'success'   => 1,
            'data'      => [
                ['queue_added' => 1, 'unique_id' => 'Portal:1', 'protocol' => 'prot-1'],
                ['queue_added' => 1, 'unique_id' => 'Portal:2', 'protocol' => 'prot-2'],
                ['queue_added' => 1, 'unique_id' => 'Portal:3', 'protocol' => 'prot-3'],
                ['queue_added' => 1, 'unique_id' => 'Portal:4', 'protocol' => 'prot-4'],
                ['queue_added' => 1, 'unique_id' => 'Portal:5', 'protocol' => 'prot-5'],
                ['queue_added' => 1, 'unique_id' => 'Portal:6', 'protocol' => 'prot-6'],
                ['queue_added' => 1, 'unique_id' => 'Portal:7', 'protocol' => 'prot-7'],
                ['queue_added' => 1, 'unique_id' => 'Portal:8', 'protocol' => 'prot-8'],
                ['queue_added' => 1, 'unique_id' => 'Portal:9', 'protocol' => 'prot-9'],
                ['queue_added' => 1, 'unique_id' => 'Portal:10', 'protocol' => 'prot-10'],
                ['queue_added' => 1, 'unique_id' => 'Portal:11', 'protocol' => 'prot-11'],
                ['queue_added' => 1, 'unique_id' => 'Portal:12', 'protocol' => 'prot-12'],
                ['queue_added' => 1, 'unique_id' => 'Portal:13', 'protocol' => 'prot-13'],
            ]
        ], $responses);
    }

    private function collection() {
        $translator = $this->portalTranslator;
        $response   = $this->vendorClient->post('condominios/get');

        return $translator->toWinkerList($response['data']);
    }

    private function read($unique_id) {
        $translator = $this->portalTranslator;
        $parsed     = \Winker\Integration\Util\Integration\UniqueId::parse($unique_id);
        $response   = $this->vendorClient->post("condominios/index/id/{$parsed['Portal']}");

        return $translator->toWinker($response['data'][0]['condominio'][0]);
    }

    private function catchAndSync($uniquesIds) {
        $portals = [];

        foreach ($uniquesIds as $uniqueId) {
            $portals[] = $this->read($uniqueId);
        }

        return $this->winkerClient->post('/v1/portal', $portals);
    }

    private function catchAndSyncAll() {
        $responses = $this->vendorClient->whileNextPage('condominios/get', function($response) {
            return $this->winkerClient->post('/v1/portal', $this->portalTranslator->toWinkerList($response['data']));
        });

        return ResponseApi::fromMultiplesResponses($responses);
    }
}