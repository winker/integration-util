<?php

class WinkerTranslatorTest extends TestCase {

    /**
     * @var PortalTranslator
     */
    private $portalTranslator;

    /**
     * @var \Winker\Integration\Util\Integration\Api
     */
    private $winkerClient;

    public function setUp() {
        $this->portalTranslator = new PortalTranslator();
        $this->winkerClient     = new \Winker\Integration\Util\Integration\Api();
    }

    public function testCollection() {
        $this->winkerClient->mockJsonResponse([
            'success'   => 1,
            'data'      => [
                [
                    'unique_id' => 'Portal:1|Sub:71',
                    'name'      => 'Portal Name',
                    'cnpj'      => '51213880000115',
                    'phone'     => '',
                    'address'    => 'Rua de Teste',
                ],
                [
                    'unique_id' => 'Portal:2|Sub:72',
                    'name'      => 'Portal 2 Name',
                    'cnpj'      => '',
                    'phone'     => '',
                    'address'    => 'Rua de Teste 2',
                ]
            ]
        ]);

        $collection = $this->collection();

        $this->assertArraySubset([
            [
                'id_condominio_cond'    => 71,
                'st_fantasia_cond'      => 'Portal Name',
                'st_cpf_cond'           => '51213880000115',
                'st_telefone_cond'      => '',
                'st_endereco_cond'      => 'Rua de Teste',
                'st_complemento_cond'   => '',
                'st_bairro_cond'        => '',
                'st_cidade_cond'        => '',
                'st_estado_cond'        => '',
                'st_cep_cond'           => '',
            ],
            [
                'id_condominio_cond'    => 72,
                'st_fantasia_cond'      => 'Portal 2 Name',
                'st_cpf_cond'           => '',
                'st_telefone_cond'      => '',
                'st_endereco_cond'      => 'Rua de Teste 2',
                'st_complemento_cond'   => '',
                'st_bairro_cond'        => '',
                'st_cidade_cond'        => '',
                'st_estado_cond'        => '',
                'st_cep_cond'           => '',
            ]
        ], $collection);
    }

    private function collection() {
        $translator = $this->portalTranslator;
        $response   = $this->winkerClient->get('/v1/portal');

        return $translator->toVendorList($response['data']);
    }

    public function testDisable() {
        $this->winkerClient->mockJsonResponse([
            'success'   => 1,
            'data'      => [
                [
                    'queue_added'   => 1,
                    'unique_id'     => "Portal:1",
                    'protocol'      => "prot-1"
                ]
            ]
        ]);

        $input = [
            'authtoken'         => '1234567890',
            'validationtoken'   => '9999999999',
            'data'              => [
                'id_condominio_cond'    => 1,
                'st_telefone_cond'      => '(48) 9999-8877',
                'st_fantasia_cond'      => 'Nome da conta',
            ],
            'tentativa' => 1
        ];

        $response = $this->disable($input);

        $this->assertArraySubset([
            'success'   => 1,
            'data'      => [
                [
                    'queue_added'   => 1,
                    'unique_id'     => "Portal:1",
                    'protocol'      => "prot-1"
                ]
            ]
        ], $response);
    }

    private function disable($input) {
        $translator = $this->portalTranslator;
        $uniqueId   = $translator->extractUniqueId($input['data']);

        return $this->winkerClient->get("/v1/portal/{$uniqueId}/disable");
    }
}