<?php

use \Winker\Integration\Util\Model\Translation;

class TranslationTest extends PHPUnit_Framework_TestCase {

    private $exampleData;
    private $exampleTranslate;

    protected function setUp() {
        $this->exampleData = [
            'id_condominio' => '123',
            'fantasia_cond' => 'Ilhas Belas',
            'nome_cond'     => 'Condomínio Residencial Ilhas Belas',
            'cnpj_cond'     => '93.329.359/0001-98',
            'telefone'      => '(48) 3123-4567',
            'endereco_cond' => 'Rua do Condomínio',
            'complemento'   => 'Ao lado do Alcantara',
            'bairro'        => 'Caminho da Moca',
            'cidade'        => 'SÃO JOSÉ',
            'estado'        => '42',
            'cep'           => '88.123-456',
        ];

        $this->exampleTranslate = [
            'unique_id'             => ['Portal' => 'id_condominio'],
            'name'                  => ['fantasia_cond', 'nome_cond'],
            'cnpj'                  => 'cnpj_cond',
            'phone'                 => 'telefone',
            'address'                => 'endereco_cond',
            'address_complement'     => 'complemento',
            'address_neighborhood'   => 'bairro',
            'address_city'           => 'cidade',
            'address_state'          => 'estado',
            'address_zipcode'        => 'cep',
        ];
    }

    public function testTranslate() {
        $translated = Translation::translate($this->exampleData, $this->exampleTranslate);

        $this->assertArraySubset([
            'unique_id'             => 'Portal:123',
            'name'                  => 'Ilhas Belas',
            'cnpj'                  => '93.329.359/0001-98',
            'phone'                 => '(48) 3123-4567',
            'address'                => 'Rua do Condomínio',
            'address_complement'     => 'Ao lado do Alcantara',
            'address_neighborhood'   => 'Caminho da Moca',
            'address_city'           => 'SÃO JOSÉ',
            'address_state'          => '42',
            'address_zipcode'        => '88.123-456',
        ], $translated);
    }

    public function testTranslateOptionTwo() {
        $this->exampleData['fantasia_cond'] = '';
        $translated = Translation::translate($this->exampleData, $this->exampleTranslate);

        $this->assertEquals('Condomínio Residencial Ilhas Belas', $translated['name']);
    }

    public function testTranslateList() {
        $data1 = $this->exampleData;
        $data1['fantasia_cond'] = 'Name 1';

        $data2 = $this->exampleData;
        $data2['fantasia_cond'] = 'Name 2';

        $translatedData1 = Translation::translate($data1, $this->exampleTranslate);
        $translatedData2 = Translation::translate($data2, $this->exampleTranslate);

        $list           = [$data1, $data2];
        $translatedList = Translation::translateList($list, $this->exampleTranslate);

        $this->assertArraySubset([$translatedData1, $translatedData2], $translatedList);
    }

    public function testUniqueId() {
        $translate = $this->exampleTranslate;
        $translate['unique_id'] = ['Portal' => 'id_condominio', 'N2' => 'fantasia_cond', 'N3' => 'cnpj_cond'];

        $translated = Translation::translate($this->exampleData, $translate);

        $this->assertArraySubset(['unique_id' => 'Portal:123|N2:Ilhas Belas|N3:93.329.359/0001-98'], $translated);
    }

    public function testUniqueId_field_not_found() {
        $translate = $this->exampleTranslate;
        $translate['unique_id'] = ['Portal' => 'invalid_field'];

        $message = null;

        try {
            Translation::translate($this->exampleData, $translate);
        } catch (Translation\Exception $exception) {
            $message = $exception->getMessage();
        }

        $this->assertEquals('unique_id need "invalid_field" field.', $message);
    }

    public function testTranslate_without_uniqueid() {
        $translate = $this->exampleTranslate;
        unset($translate['unique_id']);

        $translated = Translation::translate($this->exampleData, $translate);

        $this->assertArrayNotHasKey('unique_id', $translated);
        $this->assertArraySubset([
            'name'                  => 'Ilhas Belas',
            'cnpj'                  => '93.329.359/0001-98',
            'phone'                 => '(48) 3123-4567',
            'address'                => 'Rua do Condomínio',
            'address_complement'     => 'Ao lado do Alcantara',
            'address_neighborhood'   => 'Caminho da Moca',
            'address_city'           => 'SÃO JOSÉ',
            'address_state'          => '42',
            'address_zipcode'        => '88.123-456',
        ], $translated);
    }

    public function testClosure() {
        $translated = Translation::translate([
            'campo_2'       => 'Ok',
            'ativo_blank'   => ''
        ], [
            'field_2'               => 'campo_2',
            'enabled_blank'         => function() {
                return 1;
            },
            'enabled_without'       => function() {
                return 2;
            },
        ]);

        $this->assertArraySubset([
            'field_2'           => 'Ok',
            'enabled_blank'     => '1',
            'enabled_without'   => '2',
        ], $translated);
    }

    public function testTranslateWithBlankSpace() {

        $exampleData = [
            'id_condominio' => ' 123 ',
            'fantasia_cond' => ' Ilhas Belas ',
            'nome_cond'     => ' Condomínio Residencial Ilhas Belas ',
            'cnpj_cond'     => ' 93.329.359/0001-98 ',
            'telefone'      => ' (48) 3123-4567 ',
            'endereco_cond' => ' Rua do Condomínio ',
            'complemento'   => ' Ao lado do Alcantara ',
            'bairro'        => ' Caminho da Moca ',
            'cidade'        => ' SÃO JOSÉ ',
            'estado'        => ' 42 ',
            'cep'           => ' 88.123-456 ',
        ];

        $translated = Translation::translate($exampleData, $this->exampleTranslate);

        $this->assertArraySubset([
            'unique_id'             => 'Portal:123',
            'name'                  => 'Ilhas Belas',
            'cnpj'                  => '93.329.359/0001-98',
            'phone'                 => '(48) 3123-4567',
            'address'                => 'Rua do Condomínio',
            'address_complement'     => 'Ao lado do Alcantara',
            'address_neighborhood'   => 'Caminho da Moca',
            'address_city'           => 'SÃO JOSÉ',
            'address_state'          => '42',
            'address_zipcode'        => '88.123-456',
        ], $translated);
    }
}