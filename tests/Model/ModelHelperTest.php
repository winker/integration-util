<?php

use \Winker\Integration\Util\Model\ModelHelper;

class ModelHelperTest extends PHPUnit_Framework_TestCase {
    public function testExtractUniqueIds() {
        $models = [
            ['unique_id' => 'Portal 1', 'name' => 'Portal name 1'],
            ['unique_id' => 'Portal 2', 'name' => 'Portal name 2'],
            ['unique_id' => 'Portal 3', 'name' => 'Portal name 3'],
        ];

        $this->assertArraySubset(['Portal 1', 'Portal 2', 'Portal 3'], ModelHelper::extractUniqueIds($models));
    }
}